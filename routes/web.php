<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin-cms'], function(){

    Route::get('/', 'DashboardController@index');

    Route::group(['prefix' => 'products'], function(){
        Route::get('/', 'ProductController@index');
        Route::get('/add-product', 'ProductController@getBrandAndCategories');
        Route::post('/', 'ProductController@storeProdcut');
        Route::delete('/{productId}', 'ProductController@delete');
        Route::put('/{productId}', 'ProductController@update');
        Route::get('/get-product/{productId}', 'ProductController@getProduct');
    });


    Route::group(['prefix' => 'orders'], function(){
        Route::get('/{sStatus?}', 'OrdersController@index');
        Route::get('/edit/{orderId}', 'OrdersController@editOrder');
        Route::put('/update/{orderId}', 'OrdersController@updateOrder');

    });


    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController@index');
    });


    Route::group(['prefix'=> 'categories'], function () {
        Route::get('/', 'CategoriesController@index');
        Route::get('/add-category', 'CategoriesController@showCategoryForm');
        Route::post('/', 'CategoriesController@storeCategory');
        Route::get('add-category/{categoryId}', 'CategoriesController@showSingleCategory');
        Route::put('/{categoryId}', 'CategoriesController@update');
        Route::delete('/{categoryId}' , 'CategoriesController@delete');
    });


    Route::group(['prefix' => 'brands'], function () {
        Route::get('/' , 'BrandsController@index');
        Route::get('/add-brands', 'BrandsController@showBrandForm');
        Route::post('/', "BrandsController@storeBrand");
        Route::get('/get-brand/{brandId}', 'BrandsController@getSingleBrand');
        Route::put('/{brandId}', 'BrandsController@update');
        Route::delete('/{brandId}', 'BrandsController@delete');
    });

    Route::group(['prefix' => 'users'], function(){
        Route::get('/{type?}', 'UserController@userListing');
        Route::get('edit/{id}', 'UserController@edit');
        Route::PUT('update/{id}', 'UserController@update');
        Route::delete('delete/{id}', 'UserController@delete');
    });

});


Route::group(['prefix' => 'products'], function () {
    Route::get('/', 'ProductController@customerProductList');
    Route::get('/{productId}', 'ProductController@showSingleProduct');
    Route::post('/', 'ProductController@searchProduct');

});

Route::group(['prefix'=> 'cart', 'middleware'=> 'auth'], function (){
    Route::post('/add-to-cart', 'CartsController@addToCart');
    Route::get('/', 'CartsController@viewCart');
    Route::put('/', 'CartsController@updateCart');
    Route::delete('/', 'CartsController@deleteCart');
    Route::get('/checkout', 'OrdersController@viewCheckout');

});

Route::group(['prefix' => 'orders','middleware'=> 'auth'], function(){
    Route::post('/create-order', 'OrdersController@createOrder');
});

Route::group(['prefix' => 'users', 'middleware'=> 'auth'], function (){

    Route::get('my-orders', 'UserController@getOrderHistory');

});

Route::group(['prefix'=> 'wishlist', 'middleware'=> 'auth'], function () {
    Route::get('/store', 'WishlistController@createWishlist');
    Route::get('/', 'WishlistController@index');
    Route::delete('/delete', 'WishlistController@deleteWishList');

});

Route::group(['prefix'=> 'category'], function () {

    Route::get('/{categoryId}/products', 'CategoriesController@productByCategory');
    Route::get('get-categories', 'CategoriesController@loadCategories');
});




//Route::get('/', 'HomeController@index')->name('home');




// Route::prefix('/admin-cms/products')->group(function() {
//     Route::get('/', '');
// });

// Route::get('/admin-cms/dashboard', function () {
//     return view('admin-pnl/home');
// });


// Route::get('/admin-cms/products', function () {
//     return view('admin-pnl/products');
// });

// Route::get('/admin-cms/products/add-product', function () {
//     return view('admin-pnl/add-product');
// });

Auth::routes();


Route::get('/', 'CustomersController@index')->name('home');