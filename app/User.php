<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class User extends Authenticatable
{
    use Notifiable;

    public $oClient;

    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'exceptions' =>  true,
        ]);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];

    /**
     * @var array
     */
    protected  $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getUserListing($type)
    {
        try {
            $oResponse = $this->oClient->request('GET', 'users/'.$type);
            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $$e->getResponse(),
            ]);
        }
    }


    public function showUser($id)
    {
        try {
            $oResponse = $this->oClient->request('GET', 'users/show/'.$id);
            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getResponse(),
            ]);
        }
    }

    public function updateUser($id, $request)
    {
        $data = $request->all();

        try {
            $oResponse = $this->oClient->request('PUT', 'users/update/'.$id, [
                'form_params' => $data
            ]);

            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getResponse(),
            ]);
        }
    }

    public function deleteUser($id)
    {
        try {
            $oResponse = $this->oClient->request('DELETE', 'users/delete/'.$id);
            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getResponse(),
            ]);
        }
    }

    public static function store($data)
    {
        $user = new User();

        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->date_of_birth = $data['date_of_birth'];
        $user->contact_no = $data['contact_no'];
        $user->state = $data['state'];
        $user->postal_code = $data['postal_code'];
        $user->city = $data['city'];
        $user->shipped_address = $data['shipped_address'];
        $user->address = $data['shipped_address'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);

        $user->save();

        return $user;
    }

}
