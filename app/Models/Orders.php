<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\RequestException;

class Orders extends Model
{

    public $oClient;

    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'exceptions' =>  true
        ]);
    }


    public function listOrder()
    {
        try {

            $oResponse = $this->oClient->request('GET', 'orders/');

            return $oResponse->getBody();


        } catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status' => false,
                'body' => json_decode($oResponse->getBody()->getContents()),
            ];
        }
    }

    public function createOrder($request, $customerId)
    {
        $data = $request->all();
        $data['customer_id'] = $customerId;

        try {
            $oResponse = $this->oClient->request('POST', "orders/create", [
                'form_params' => $data
            ]);
            return $oResponse->getBody();

        } catch (RequestException $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
    }


    public function getOrderHistory($customerId) {

        try {
            $oResponse = $this->oClient->request('GET', "users/order-history/". (int) $customerId);
            return $oResponse->getBody();

        } catch (RequestException $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }

    }

    public function showOrder($orderId)
    {
        try {
            $oResponse = $this->oClient->request('GET', "orders/show/". $orderId);
            return $oResponse->getBody();

        } catch (RequestException $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
    }

    public function getOrderStatuses()
    {
        try {
            $oResponse = $this->oClient->request('GET', "orders/statuses");
            return $oResponse->getBody();

        } catch (RequestException $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
    }


    public function updateOrder($orderId, $request)
    {
        $data = $request->all();

        try {
            $oResponse = $this->oClient->request('PUT', "orders/update/".$orderId, [
                'form_params' => $data
            ]);
            return $oResponse->getBody();

        } catch (RequestException $e) {

            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
    }


    public function getStatusCount() {

        try{

            $oResponse = $this->oClient->request('GET', 'orders/order-status-count');

            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
