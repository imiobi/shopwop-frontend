<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Brands
 * @package App\Models
 */
class Brands extends Model
{
    /**
     * @var Client
     */
    public $oClient;

    /**
     * Brands constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'timeout' => 2.0,
            'exceptions' =>  true
        ]);
    }


    /**
     * @return array|mixed
     */
    public function listBrands() {

        try{
            $oResponse = $this->oClient->request('GET', 'brands');

            return json_decode($oResponse->getBody());


        }catch(GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }


    /**
     * @return array|mixed
     */
    public function brandsWithCount() {

        try{
            $oResponse = $this->oClient->request('GET', 'brands/count-products-by-brand');

            return json_decode($oResponse->getBody());


        }catch(GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }


    /**
     * @param $aPostedData
     * @return array|mixed
     */
    public function storeBrand($aPostedData) {

        try{
            $oResponse = $this->oClient->request('POST', 'brands/create', [
                'form_params' => $aPostedData
            ]);

            return json_decode($oResponse->getBody());


        }catch(GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'errors'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }


    /**
     * @return array
     */
    public function getCountries() {

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://restcountries.eu/rest/v2',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        try{
            $oResponse = $client->request('GET', "/all");

            dd($oResponse);


        }catch(GuzzleException $e) {

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();

            dd($responseBodyAsString);
            $oResponse = $e->getResponse();
            echo json_decode($oResponse->getBody()->getContents());
            die;
            return [
                'status'=> false,
                'errors'=> json_decode($oResponse->getBody()->getContents())
            ];
        }

    }

    /**
     * @param $brandId
     * @return array|mixed\
     */
    public function getBrand($brandId) {

        try{

            $oResponse = $this->oClient->request('GET', "brands/show/". $brandId);

            return json_decode($oResponse->getBody());

        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'errors'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }

    /**
     * @param $aPostedData
     * @param $brandId
     * @return array|mixed
     */
    public function updateBrand($aPostedData, $brandId) {

        try{

            $oResponse = $this->oClient->request('PUT', 'brands/update/' . $brandId, [
                'form_params' => $aPostedData,
            ]);

            return json_decode($oResponse->getBody());

        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'errors'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }


    /**
     * @param $brandId
     * @return array|mixed
     */
    public function deleteBrand($brandId) {

        try{

            $oResponse = $this->oClient->request('DELETE', 'brands/delete/' . $brandId);

            return json_decode($oResponse->getBody());

        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'errors'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }



    public function getBrandStats() {

        try{

            $oResponse = $this->oClient->request('GET', 'brands/stats');

            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'errors'=> $oResponse->getBody()->getContents(),
            ];
        }
    }
}
