<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Wishlist extends Model
{
    public $oClient;

    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'exceptions' =>  true
        ]);
    }


    public function storeWishList($productId,$iCustomerId) {

        try {

            $aPostData = [
                'product_id'=> (int)$productId,
                'customer_id' => $iCustomerId,
            ];

//            return $aPostData;

            $oResponse = $this->oClient->request('POST', 'wishlist/create', [
                'form_params'=> $aPostData,
            ]);

            return $oResponse->getBody();


        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents()),
            ];
        }

    }


    public function listWishList($iCustomerId){


        try {

            $oResponse = $this->oClient->request('GET', 'wishlist/'. $iCustomerId);

            return $oResponse->getBody();


        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents()),
            ];
        }

    }


    public function deleteWishList($product_id, $customer_id) {

        try {

            $data['customer_id'] = $customer_id;
            $data['product_id'] = $product_id;

            $oResponse = $this->oClient->request('DELETE', 'wishlist/delete', ['form_params' => $data]);

            return $oResponse->getBody();


        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents()),
            ];
        }

    }
}
