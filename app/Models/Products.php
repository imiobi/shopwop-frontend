<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\File;
use GuzzleHttp\Exception\RequestException;

class Products extends Model
{

    public $oClient;

    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'timeout' => 2.0,
            'exceptions' =>  true
        ]);
    }

    public function listProduct($pageNumber = null, $perPage = null, $sortBy = null, $orderBy = null) {
        try{

            if($pageNumber == null && $perPage == null && $sortBy == null && $orderBy == null) {
                $oResponse = $this->oClient->request('GET', 'products');
            } else {
                $oResponse = $this->oClient->request('GET', 'products?perPage='.$perPage.'&page='.$pageNumber. '&sortBy='. $sortBy . '&orderBy='. $orderBy );
            }
            

            $oData = json_decode($oResponse->getBody());

            if($oData->status === true) {
                return $oData;
            }   

        }catch(GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents())
            ];
        }
    }

    public function insertProduct($aPostedData) {

        try {


            $oResponse = $this->oClient->request('POST', "products/create", [

                'form_params' => $aPostedData
            ]);

            return $oResponse->getBody();

        } catch (RequestException $e) {

            return $e->getMessage();
        }
    }


    public function getBrandCategory() {

        try{
            $data = [];
            $oResponse = $this->oClient->request('GET', 'brands');

            $oResponse = json_decode($oResponse->getBody());

            if($oResponse->status === true) {

                $data['brands'] = $oResponse->data;


                try {
                    $oResponse = $this->oClient->request('GET', 'categories');

                    $data['categories'] = json_decode($oResponse->getBody())->data;

                    return $data;
                }catch (GuzzleException $e) {
                    $oResponse = $e->getResponse();
                    return [
                        'status'=> false,
                        'body'=> json_decode($oResponse->getBody()->getContents()),
                    ];
                }
            }

        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents()),
            ];
            die();
        }
    }


    /**
     *
     */
    public function deleteProduct($productId) {

        try {

            $oResponse = $this->oClient->request('DELETE', 'products/delete/'.$productId);

            return $oResponse->getBody();


        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents()),
            ];
        }
    }


    public function getProduct($productId) {


        try {

            $oResponse = $this->oClient->request('GET', 'products/show/'.$productId);

            return $oResponse->getBody();


        }catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return [
                'status'=> false,
                'body'=> json_decode($oResponse->getBody()->getContents()),
            ];
        }
    }


    public function updateProduct($aPostedData, $productId) {
        $aPostedData['price'] = (float) $aPostedData['price'];
        $aPostedData['stock_quantity'] = intval($aPostedData['stock_quantity']);

        try {


            $oResponse = $this->oClient->request('PUT', 'products/update/' . $productId, [
                'form_params' => $aPostedData
            ]);

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }
    }



    public function getRecentProducts() {

        try {


            $oResponse = $this->oClient->request('GET', 'products/top-4-recent-products');

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }

    }


    public function getTopSoldProducts() {
        try {


            $oResponse = $this->oClient->request('GET', 'products/top-4-sold-products');

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }
    }


    public function getTopViewedProducts() {

        try {


            $oResponse = $this->oClient->request('GET', 'products/top-12-most-viewed-products');

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }

    }



    public function getTopCategoryProducts() {
        try {

            $oResponse = $this->oClient->request('GET', 'categories/top-5-categories');

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }

    }


    public function searchProductOnQuery($sKeyword, $pageNumber, $perPage, $sortBy, $orderBy) {

        try {

            $oResponse = $this->oClient->request('GET', 'products/search?q='.$sKeyword.'&page='.$pageNumber.'&perPage='.$perPage. '&orderBy=' .$orderBy. '&sortBy=' .$sortBy);
            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }

    }


    public function getProductStats() {

        try{

            $oResponse = $this->oClient->request('GET', 'products/stats');
            return $oResponse->getBody();

        }catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }
    }



}
