<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Categories extends Model
{

    public $oClient;

    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'timeout' => 2.0,
            'exceptions' =>  true
        ]);

    }

    public function listCategories() {

        try{

            $oResponse = $this->oClient->request('GET', 'categories/');

            return json_decode($oResponse->getBody());


        }catch (GuzzleException $e) {

            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }
    }


    public function CategriesWithCount() {

        try{

            $oResponse = $this->oClient->request('GET', 'categories/count-products-by-category/');

            return json_decode($oResponse->getBody());


        }catch (GuzzleException $e) {

            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }
    }



    public function storeCategroy($aPostedData) {

        try {
            $oResponse = $this->oClient->request('POST', 'categories/create',[
                'form_params' => $aPostedData,
            ]);

            return json_decode($oResponse->getBody());

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }
    }



    public function getSingleCategory($categoryId) {
        try {
            $oResponse = $this->oClient->request('GET', 'categories/show/' . $categoryId);

            return json_decode($oResponse->getBody());

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }
    }



    public function updateCategory($categoryId, $aPostedData) {

        try{
            $oResponse = $this->oClient->request('PUT', 'categories/update/' . $categoryId, [
                'form_params' => $aPostedData
            ]);

            return json_decode($oResponse->getBody());

        }catch (GuzzleException $e) {

            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);

        }

    }



    public function deleteCategory($categoryId) {

        try{
            $oResponse = $this->oClient->request('DELETE', 'categories/delete/' . $categoryId);

            $oResponse =  json_decode($oResponse->getBody());

            return $oResponse;

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }
    }


    public function getCategoriesStats() {

        try{
            $oResponse = $this->oClient->request('GET', 'categories/stats');

            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }
    }



    public function getCategoryProduct($categoryId,  $pageNumber = null, $perPage = null, $sortBy = null, $orderBy = null) {

        try{
            $oResponse = $this->oClient->request('GET', 'categories/'.$categoryId.'/products?perPage='.$perPage.'&page='.$pageNumber. '&sortBy='. $sortBy . '&orderBy='. $orderBy);

            return $oResponse->getBody();

        }catch (GuzzleException $e) {
            return response()->json([
                'status'=> false,
                'error'=> $e->getMessage(),
            ]);
        }

    }

}
