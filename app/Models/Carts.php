<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;

class Carts extends Model
{

    public $oClient;


    public function __construct()
    {
        parent::__construct();

        $this->oClient = new Client([
            'base_uri' => env('SYSTEM_API_URL'),
            'exceptions' =>  true,
        ]);
    }

    public function createCart($aPostedData) {

        try {


            $oResponse = $this->oClient->request('POST', 'carts/add-to-cart', [
                'form_params' => $aPostedData
            ]);

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {
            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse->getBody()->getContents(),
            ]);
        }
    }



    public function getCart($customerId) {

        try {


            $oResponse = $this->oClient->request('GET', 'carts/carts/'. $customerId);

            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getResponse();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse->getBody()->getContents(),
            ]);
        }
    }


    public function updateCart($aCart) {

        try {


            $oResponse = $this->oClient->request('PUT', 'carts/update-cart' ,[
                'form_params' => $aCart
            ]);


            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getMessage();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }

    }


    public function deleteCart($cartId) {

        try {


            $oResponse = $this->oClient->request('DELETE', 'carts/delete/'. $cartId);


            return $oResponse->getBody();

        }
        catch (GuzzleException $e) {

            $oResponse = $e->getMessage();
            return response()->json([
                'status'=> false,
                'error'=> $oResponse,
            ]);
        }
    }
}
