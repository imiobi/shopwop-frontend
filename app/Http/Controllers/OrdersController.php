<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\Orders;
use Illuminate\Http\Request;
use Auth;

class OrdersController extends Controller
{

    public function createOrder(Request $request)
    {

        $oOrder = new Orders;
        $oResponse = $oOrder->createOrder($request, Auth::user()->id);

        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {

            return redirect()->back()->with('success', $oResponse->message);
        }

        return redirect()->back()->withErrors('errors', "Sorry, Something unexpected happened you order wad not placed");
    }

    public function viewCheckout () {

        $iGrandTotal = 0;

        $oCart = new Carts();

        $aCustomer = Auth::user();

        $oResponse = $oCart->getCart($aCustomer->id);

        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {

            $aStates = ['Sindh', 'Punjab', 'baluchistan', 'Khyber Pakhtunkhwa'];

            foreach ($oResponse->data as $cart) {
                $iSubtotal = $cart->quantity_ordered * $cart->cart_product->price;

                $iGrandTotal += $iSubtotal;
            }


            return view('checkout' , [
                'aOrders' => $oResponse->data,
                'iGrandTotal' => $iGrandTotal,
                'aCustomer' => $aCustomer,
                'aStates' => $aStates,
            ]);
        }

    }


    public function index($sStatus = null) {


        $oOrder = new Orders();

        $oResponse = $oOrder->listOrder();

        $oResponse = json_decode($oResponse);

        $aSelectedData = [];

        if($oResponse->status == true) {

            $aOrders = $oResponse->data;

            if(isset($sStatus) && !empty($sStatus)) {

                foreach ($aOrders as $oOrder) {


                    if($oOrder->status == $sStatus) {
                        $aSelectedData[] = $oOrder;
                    }
                }

                return view('admin-pnl.orders', ['oOrders' => $aSelectedData]);

            }else {
                return view('admin-pnl.orders', ['oOrders' => $oResponse->data]);

            }

        }
    }


    public function editOrder($orderId)
    {
        $orderId = decrypt($orderId);

        $oOrder = new Orders;
        $oResponse = $oOrder->showOrder($orderId);
        $aStatuses = json_decode($oOrder->getOrderStatuses());
        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {
            return view('admin-pnl.update-order', ['aOrder' => $oResponse->data, 'aStatuses' => $aStatuses->data]);
        } else {
            return 'response error';
        }
    }

    public function updateOrder($orderId, Request $request)
    {
        $orderId = decrypt($orderId);

        $oOrder = new Orders;
        $oResponse = $oOrder->updateOrder($orderId, $request);

        $oResponse = json_decode($oResponse);


        if($oResponse->status == true) {
            return redirect()->back()->with('success', $oResponse->message);
        } else {
            return redirect()->back()->withErrors($oResponse->errors);
        }

    }
}
