<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use Illuminate\Http\Request;

class BrandsController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index() {

        $oBrand = new Brands();

        $oResponse = $oBrand->listBrands();

        if($oResponse->status == true) {
            return view ('admin-pnl.brands', ['aBrands'=> $oResponse->data]);
        }

        return redirect('admin-pnl.brands')->withErrors('error', $oResponse->error);
    }



    public function showBrandForm() {

        return view('admin-pnl.add-brands');
    }


    public function storeBrand(Request $request) {

        $aPostedData = $request->except('_method', '_token');


        $oBrand = new Brands();

        $oResponse = $oBrand->storeBrand($aPostedData);

        if($oResponse->status == true) {

            return redirect()->back()->with('success', $oResponse->message);
        }

        return redirect()->back()->withErrors($oResponse->errors);

    }



    public function getSingleBrand($brandId) {

        $oBrand = new Brands();

        $oResponse = $oBrand->getBrand(decrypt($brandId));

        if($oResponse->status == true) {

            return view('admin-pnl.add-brands', ['oBrandData' => $oResponse->data ]);
        }

        return redirect('admin-cms/brands/add-brands')->withErrors('errors', $oResponse->errors);
    }


    public function update(Request $request, $brandId) {

        $aPostedDate = $request->except('_token', '_method');

        $oBrand = new Brands();

        $oResponse = $oBrand->updateBrand($aPostedDate , decrypt($brandId));

        if($oResponse->status == true) {

            return redirect()->back()->with('success', $oResponse->message);
        }

        return redirect()->back()->withErrors($oResponse->errors);

    }


    public function delete($brandId) {

        $oBrand = new Brands();

        $oResponse = $oBrand->deleteBrand(decrypt($brandId));

        if($oResponse->status == true) {

            return redirect('admin-cms/brands')->with('success', $oResponse->message);
        }

        return redirect()->back()->withErrors('errors', $oResponse->errors);

    }

}
