<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use App\Models\Categories;
use App\Models\Orders;
use App\Models\Products;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {

        $oOrder = new Orders();

        $aStatusCount = json_decode($oOrder->getStatusCount());

        $oProduct = new Products();

        $oProductStats = json_decode($oProduct->getProductStats());

        $oBrand = new Brands();

        $oBrandsStats = json_decode($oBrand->getBrandStats());

        $oCategory = new Categories();

        $oCategoryStats = json_decode($oCategory->getCategoriesStats());

        if(isset($aStatusCount) && isset($oProductStats) && isset($oBrandsStats) && isset($oCategoryStats)) {

            if($aStatusCount->status == true && $oProductStats->status == true && $oBrandsStats->status == true && $oCategoryStats->status == true) {

                return view('admin-pnl.home', [
                    'aStatusCounts' => json_decode(json_encode($aStatusCount->data), true),
                    'aProductStats' => $oProductStats->data,
                    'aBrandStats'   => $oBrandsStats->data,
                    'aCategoryStats'   => $oCategoryStats->data,
                ]);

            }else {

                abort(419);
            }
        }else {

            return view('admin-pnl.home', 'error', 'Something unexpected happened');
        }

    }






}
