<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use App\Models\Categories;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use App\Models\Products;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $objProduct = new Products();
        $oProducts = $objProduct->listProduct($pageNumber =  null, $perPage =  null, $sortBy = null, $orderBy = null);

        return view('admin-pnl.products', ['oProducts'=> $oProducts->data]);
        
    }


    public function customerProductList(Request $request) {

        $pageNumber = $request->input('page', 1);
        $perPage = $request->input('perPage', 12);
        $sortBy = $request->input('sortBy', "product_title");
        $orderBy = $request->input('orderBy', 'desc');
        
        $objProduct = new Products();
        $oProducts = $objProduct->listProduct($pageNumber, $perPage, $sortBy, $orderBy);

        $oCategories = new Categories();
        $aCategoryResponse = $oCategories->CategriesWithCount();

        $oBrand = new Brands();
        $aBrands = $oBrand->brandsWithCount();

        if($oProducts->status == true && $aCategoryResponse->status == true && $aBrands->status == true) {
            return view('products', [
                'oProducts'=> $oProducts->data,
                'aCategories'=> $aCategoryResponse->data,
                'aBrands' => $aBrands->data,
                'pageNumber' => $pageNumber,
                'sortBy' => $sortBy,
                'orderBy' => $orderBy
            ]);
        }


    }



    public function searchProduct(Request $request) {

        $sKeyword = $request->input('q');
        $pageNumber = $request->input('page', 1);
        $perPage = $request->input('perPage', 12);
        $sortBy = $request->input('sortBy', "product_title");
        $orderBy = $request->input('orderBy', 'desc');

        $objProduct = new Products();

        if(empty($sKeyword) || is_null($sKeyword) || trim($sKeyword) == '') {
            $oProducts = $objProduct->listProduct($pageNumber, $perPage, $sortBy, $orderBy);
        } else {
            $oProducts = json_decode($objProduct->searchProductOnQuery($sKeyword, $pageNumber, $perPage, $sortBy, $orderBy));
        }

        $oCategories = new Categories();
        $aCategoryResponse = $oCategories->CategriesWithCount();

        $oBrand = new Brands();
        $aBrands = $oBrand->brandsWithCount();


        if($oProducts->status == true && $aCategoryResponse->status == true && $aBrands->status == true) {

            return view('products', [
                'oProducts'=> $oProducts->data,
                'pageNumber' => $pageNumber,
                'aCategories'=> $aCategoryResponse->data,
                'aBrands' => $aBrands->data,
                'sortBy' => $sortBy,
                'orderBy' => $orderBy
            ]);
        }

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeProdcut(Request $request) {

        $validate = $request->validate([
            'image' => 'required|mimes:jpeg,bmp,png,jpg,gif,svg|max:2042|dimensions:min_width=100,min_height=100|dimensions:max_width=2000,max_height=2000',
        ]);

        $aPostedData = $request->all();

        if($request->hasFile('image')) {

            $image = $request->file('image');


            $fileName = time() . '-pic.' . $image->getClientOriginalExtension();

            $aPostedData['image'] = $fileName;
        }

        $oProduct = new Products();

        $response = $oProduct->insertProduct($aPostedData);

        $response = json_decode($response);


        if($response->status === false) {
            return redirect('/admin-cms/products/add-product')->withErrors($response->errors);
        }

        if($request->hasFile('image')) {

            $image = $request->file('image');

            $fileName = time().'-pic.'.$image->getClientOriginalExtension();

            //Single pic
            Image::make($image)->resize(null, 603, function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/single/'.$fileName);

            //Catalog pic
            Image::make($image)->resize(null, 273, function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/img/'.$fileName);

            //thumbnail image to be saved here
            Image::make($image)->resize(60, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save('storage/thumbnail/'.$fileName);

            return redirect('/admin-cms/products/add-product')->with('Success', 'Product has been saved');

        }

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBrandAndCategories() {

        $objProduct = new Products();

        $aBrandCategories = $objProduct->getBrandCategory();

        $brands = $aBrandCategories['brands'];
        $categories = $aBrandCategories['categories'];

        return view('admin-pnl.add-product', ['brands'=> $brands, 'categories'=> $categories]);
    }


    /**
     * @param $productId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($productId) {

        $objProduct = new Products();

        $jsonResponse = $objProduct->deleteProduct(decrypt($productId));

        $jsonResponse = json_decode($jsonResponse);

        if($jsonResponse->status == true) {
            return redirect('admin-cms/products')->with('success' ,'Record Deleted Successfuly');
        }

        return redirect('admin-cms/products')->with('error', $jsonResponse->message);
    }


    /**
     *
     */
    public function getProduct($productId) {
        $objProduct = new Products();

        $oResponse = $objProduct->getProduct(decrypt($productId));


        $oJsonResponse = json_decode($oResponse);

        if($oJsonResponse->status == true) {

            $aBrandCategories = $objProduct->getBrandCategory();

            $brands = $aBrandCategories['brands'];
            $categories = $aBrandCategories['categories'];

            return view('admin-pnl.add-product', [
                'brands'=> $brands,
                'categories'=> $categories,
                'oProduct'=> $oJsonResponse->data
            ]);
        }

        return redirect('admin-cms/products')->with('error', $oJsonResponse->message);

    }


    /**
     * @param $productId
     */
    public function update(Request $request, $productId)
    {

        $image = null;
        $fileName = null;

//        $request->validate([
//            'image' => 'required|mimes:jpeg,bmp,png,jpg,gif,svg|max:2042|dimensions:min_width=100,min_height=100|dimensions:max_width=2000,max_height=2000',
//        ]);

        $aPostedData = $request->except('_method','_token');

        if ($request->hasFile('image')) {

            $image = $request->file('image');

            $fileName = time() . '-pic.' . $image->getClientOriginalExtension();

            $aPostedData['image'] = $fileName;
        }

        $objProduct = new Products();

        $oResponse = $objProduct->updateProduct($aPostedData, decrypt($productId));


        $oResponse = json_decode($oResponse);


        if ($oResponse->status == true) {

            if ($request->hasFile('image')) {


                //Single pic
                Image::make($image)->resize(null, 603, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('storage/single/'.$fileName);

                //orignal pic
                Image::make($image)->resize(null, 273, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('storage/img/' . $fileName);

                //thumbnail image to be saved here
                Image::make($image)->resize(60, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save('storage/thumbnail/' . $fileName);

                return redirect('admin-cms/products')->with('success', "Record Updated Successfuly");
            }

            return redirect('admin-cms/products')->withErrors('error', $oResponse->message);

        }
    }


    public function showSingleProduct($productId) {

        $objProduct = new Products();

        $oResponse = $objProduct->getProduct(decrypt($productId));


        $oJsonResponse = json_decode($oResponse);



        if($oJsonResponse->status == true) {

            $aBrandCategories = $objProduct->getBrandCategory();

            $oCatregories = new Categories();

            $category = $oCatregories->getSingleCategory($oJsonResponse->data->category_id);

            return view('single-product', [
                'oCategory'=> isset($category->data) ? $category->data : null,
                'oProduct'=> $oJsonResponse->data
            ]);
        }
    }




}
