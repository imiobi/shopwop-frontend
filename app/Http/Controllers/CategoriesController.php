<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    /**
     *
     */
    public function index() {

        $oCatregories = new Categories();

        $oResponse = $oCatregories->listCategories();
        $oCatregories = $oResponse->data;
        if($oResponse->status == true) {
            return view('admin-pnl.categories', ['aCategories' => $oCatregories]);
        }

        return view('admin-pnl.categories', ['aCategories' => false]);
    }


    public function showCategoryForm() {

        $oCatregories = new Categories();

        $oResponse = $oCatregories->listCategories();

        if($oResponse->status == true) {

            return view('admin-pnl.add-category', ['aCategories' => $oResponse->data]);

        }

        return view('admin-pnl.categories', ['aCategories' => false]);

    }



    public function storeCategory(Request $request) {

        $aPostedData =  $request->except('_token', "_method");

        $oCategory = new Categories();

        $oResponse = $oCategory->storeCategroy($aPostedData);

        if($oResponse->status == true) {
            return redirect()->back()->with('success', $oResponse->message);
        }

        return redirect()->back()->withErrors($oResponse->errors);
    }


    public function showSingleCategory($categoryId) {

        $oCatregories = new Categories();

        $oResponse = $oCatregories->getSingleCategory(decrypt($categoryId));

        if($oResponse->status == true) {
            $aCategories = $oCatregories->listCategories();

            return view('admin-pnl.add-category', [
                'aCategories' => $aCategories->data,
                'oCategoryData' => $oResponse->data,
                ]);
        }

        return redirect('admin-cms/categories')-back()->withErrors('error', $oResponse->error);
    }


    public function update(Request $request, $categoryId) {

        $aPostedData = $request->except('_method', '_token');

        $oCategories = new Categories();

        $oResponse = $oCategories->updateCategory(decrypt($categoryId), $aPostedData);

        if($oResponse->status == true) {
            return redirect()->back()->with('success', $oResponse->message);
        }

        return redirect()->back()->withErrors($oResponse->errors);
    }



    public function delete($categoryId) {

        $oCategories = new Categories();

        $oResponse = $oCategories->deleteCategory(decrypt($categoryId));

        if($oResponse->status == true) {

            return redirect()->back()->with('success', $oResponse->message);
        }

        return redirect()->back()->with('error', $oResponse->message);
    }



    public function productByCategory($categoryId, Request $request) {

        $pageNumber = $request->input('page', 1);
        $perPage = $request->input('perPage', 12);
        $sortBy = $request->input('sortBy', "product_title");
        $orderBy = $request->input('orderBy', 'desc');
        
        $oCategory = new Categories();
        $oResponse = json_decode($oCategory->getCategoryProduct($categoryId, $pageNumber, $perPage, $sortBy, $orderBy));

        if(isset($oResponse) && !empty($oResponse)) {

            if($oResponse->status == true) {

                return view('categories', [
                                            'aCategoryProducts'=> $oResponse->data,
                                            'pageNumber' => $pageNumber,
                                            'sortBy' => $sortBy,
                                            'orderBy' => $orderBy
                                          ]
                );

            }
        }



    }


    public function getAllCategories() {


    }


    public function loadCategories() {

        $oCategory = new Categories();

        $oResponse = $oCategory->CategriesWithCount();

        if(isset($oResponse) && !empty($oResponse)) {

            if($oResponse->status == true) {

                return $oResponse->data;
            }
        }
    }



}
