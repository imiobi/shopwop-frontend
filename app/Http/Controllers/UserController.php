<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    public function userListing($type = null)
    {
        $oUser = new User;

        if(is_null($type)) {
            $type = 'all';
        }

        $oResponse = $oUser->getUserListing($type);
        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {
            return view('admin-pnl.users', ['aUsers' => $oResponse->data]);
        }

    }

    public function edit($id)
    {
       $id = decrypt($id);

       $oUser = new User;
       $oResponse = $oUser->showUser($id);
        
       $oResponse = json_decode($oResponse);
    
       if($oResponse->status == true) {
           return view('admin-pnl.update-user', ['aUser' => $oResponse->data]);
       }
    }


    public function update($id, Request $request)
    {
        $id = decrypt($id);

        $oUser = new User;
        $oResponse = $oUser->updateUser($id, $request);

        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {
            return redirect()->back()->with('success', $oResponse->message);
        } else {
            return redirect()->back()->withErrors($oResponse->errors);
        }

    }

    public function delete($id)
    {
        $id = decrypt($id);

        $oUser = new User;
        $oResponse = $oUser->deleteUser($id);

        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {
            return redirect()->back()->with('success', $oResponse->message);
        } else {
            return redirect()->back()->withErrors($oResponse->errors);
        }
        
    }



    public function getOrderHistory() {

        $oOrder = new Orders();

        $oResponse =  $oOrder->getOrderHistory(Auth::user()->id);

        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {

            return view('order-history', ['aOrders' => $oResponse->data]);
        }

    }
}
