<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Auth;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $oProduct = new Products();

        $oRecentProducts = json_decode($oProduct->getRecentProducts());

        $oTopSoldProducts = json_decode($oProduct->getTopSoldProducts());

        $oTopViewedProducts = json_decode($oProduct->getTopViewedProducts());

        $oTopCategoryProducts = json_decode($oProduct->getTopCategoryProducts());

        return view('index', [
            'aRecentProducts' => $oRecentProducts->data,
            'aTopSoldProducts' => $oTopSoldProducts->data,
            'aTopViewedProducts' => $oTopViewedProducts->data,
            'aTopCategoryProducts' => $oTopCategoryProducts->data,
        ]);
    }
}
