<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Auth;

class WishlistController extends Controller
{


    public function createWishlist(Request $request) {


        $oWishlist = new Wishlist();

        $oResponse = $oWishlist->storeWishList($request->productId, Auth::user()->id);

        return $oResponse;
    }

    public function index() {

        $oWishlist = new Wishlist();

        $oResponse = $oWishlist->listWishList(Auth::user()->id);



        $aWishlists = json_decode($oResponse);

        $oProduct = new Products();

        if($aWishlists->status == true) {

            $aProductData = [];
            foreach ($aWishlists->data as $oWishlist) {

                $response = $oProduct->getProduct($oWishlist->product_id);

                $aProductData[] = json_decode($response);

            }


            return view('wishlist', [
                'aProductData'=> $aProductData,
            ]);
        }
    }


    public function deleteWishList(Request $request) {

        $oWishList = new Wishlist();

        $oResponse = $oWishList->deleteWishList($request->product_id, Auth::user()->id);

        return $oResponse;
    }
}
