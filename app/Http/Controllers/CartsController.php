<?php

namespace App\Http\Controllers;
use App\Models\Carts;
use Illuminate\Http\Request;
use Auth;

class CartsController extends Controller
{

    public function viewCart()
    {

        $iGrandTotal = 0;

        $oCart = new Carts();

        $oResponse = $oCart->getCart(Auth::user()->id);

        $oResponse = json_decode($oResponse);

        if ($oResponse->status == true) {

//            dd($oResponse->data);

            foreach ($oResponse->data as $cart) {
                $iSubtotal = $cart->quantity_ordered * $cart->cart_product->price;

                $iGrandTotal += $iSubtotal;
            }

            return view('cart', [
                'aCarts' => $oResponse->data,
                'iGrandTotal' => number_format($iGrandTotal),
                ]);

        }

        return redirect()->back()->withErrors('errors', $oResponse->message);


    }


    public function addToCart(Request $request) {


        $aPostedData = $request->except('_token', '_method');

        $oCart = new Carts();


        $oResponse = $oCart->createCart($aPostedData);

        $oResponse = json_decode($oResponse);

        if($oResponse->status == true) {

            return $oResponse->message;
        }


    }



    public function updateCart(Request $request)
    {


        $aData = $request->only('qty', 'identifier');

        $aCart = [];


        foreach ($aData['qty'] as $iKey => $iItem) {
            $aCart[] = [
                'quantity_ordered' => $iItem,
                'id' => isset($aData['identifier'][$iKey]) ? decrypt($aData['identifier'][$iKey]) : null
            ];
        }


        $oCart = new Carts();

        $oResponse = $oCart->updateCart($aCart);

       return $oResponse;

    }


    public function deleteCart(Request $request) {

        $carId = $request->cartId;

        $oCart = new Carts();

        $oResponse = $oCart->deleteCart($carId);

        return $oResponse;
    }

}
