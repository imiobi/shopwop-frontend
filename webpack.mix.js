const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/main.js', 'public/js/');

// mix.style('resources/css/main.css', 'public/css/');

mix.js('resources/admin-cms/js/fastclick.js', 'public/admin-assets/js/Fastclick/');
mix.js('resources/admin-cms/js/demo.js', 'public/admin-assets/js/demo/');

mix.styles(['resources/css/style.css'],'public/css/style/full.css');