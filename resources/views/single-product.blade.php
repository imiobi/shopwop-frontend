@extends('layouts.master')

@section('head')

    <link rel="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" href="sweetalert2.min.css">

@endsection()

@section('customeCSS')

    <style>
        .owl-dots .active {
            display: none;
        }
    </style>
@endsection

@section('content')

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                <div class="col-first">
                    <h1>Single Product Page</h1>

                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->


    <!-- Start Product Detail -->
    @php($color = ($oProduct->stock_quantity <= 0) ? "color : #d20" : "color : #228B22")
    <div class="container">
        <div class="product-quick-view">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="quick-view-carousel-details">
                        <div class="item" style="background: url('{{ isset($oProduct->image[0]) ?  asset('storage/single/'.$oProduct->image[0]->image_name) : ''}}');">
                        </div>

                    </div>
                </div>
                @php( $aVariants = isset($oProduct->variants) ? explode(',',$oProduct->variants) : null)
                <div class="col-lg-6">
                        <div class="quick-view-content">
                            <div class="top">
                                <h3 class="head"><a href="{{url('products/'.encrypt($oProduct->id))}}"> {{ucwords($oProduct->product_title)}} </a></h3>
                                <div class="price d-flex align-items-center"><span class="lnr lnr-tag"></span> <span class="ml-10">${{$oProduct->price}}</span></div>
                                <div class="category">Category: <span>{{!empty($oCategory) ? $oCategory->category_name : "n/a"}}</span></div>
                                <div class="available">Availibility: <span style="{{$color}}">{{($oProduct->stock_quantity) > 0 ? "In Stock" : "Out of Stock"}}</span></div>

                            </div>
                            <div class="middle">
                                <p class="content">{{$oProduct->short_description}}.</p>
                            </div>
                            @if(isset($aVariants) && !empty($aVariants))
                            <div class="middle">
                                <p class="content">Please select the Variant:</p>
                                @foreach($aVariants as $variant)
                                    <input type="radio" id="variant" class="variant" name="variant" value="{{$variant}}">
                                    <span style="background-color: {{$variant}}; width: 8px; padding: 9px; margin: 5px"></span>
                                @endforeach
                            </div>
                            @endif
                            <div >
                                <div class="quantity-container d-flex align-items-center mt-15">
                                    Quantity:
                                    <input type="text" id="quantity" class="quantity-amount ml-15" value="1" />
                                    <div class="arrow-btn d-inline-flex flex-column">
                                        <button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
                                        <button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
                                    </div>

                                </div>
                                <div class="d-flex mt-20">

                                    <form id="addToCart" method="POST" action="/cart/add-to-cart">
                                        @csrf
                                        <input type="hidden" name="customer_id" value="{{isset(Auth::user()->id) ? Auth::user()->id : ""}}" />
                                        <input type="hidden" name="product_id" value="{{$oProduct->id}}" />
                                        <input type="hidden" name="quantity_ordered" id="qty_value" value="" />
                                        <input type="hidden" name="variants" id="varaints_value" value="" />
                                        @if($oProduct->stock_quantity > 0)
                                            <button type="submit" id="cart" class="view-btn color-2"><span>Add to Cart</span></button>
                                        @endif
                                    </form>

                                    <a href="#" id="likeBtn" data-id="{{$oProduct->id}}" class="like-btn heart"><span class="lnr lnr-heart"></span></a>
                                </div>
                                <span style="color: #d0211c" id="cartErr"></span>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
<p></p>
    <div class="container">
        <div class="details-tab-navigation d-flex justify-content-center mt-30">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li>
                    <a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-expanded="true">Description</a>
                </li>
                <li>
                    <a class="nav-link" id="specification-tab" data-toggle="tab" href="#specification" role="tab" aria-controls="specification">Specification</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description">
                <div class="description">
                    <p>{{$oProduct->product_description}}</p>
                </div>
            </div>
            <div class="tab-pane fade show active" id="specification" role="tabpanel" aria-labelledby="specification">
                <div class="specification-table">

                    <div class="align-center" style="text-align: center">
                    {!! isset($oProduct->specifications) && !empty($oProduct->specifications) ? $oProduct->specifications : "No Specifications Found" !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Product Details -->




@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

    <script>
        $(document).ready(function(){

            let stockQty = <?php echo $oProduct->stock_quantity; ?>

            console.log(stockQty);

            $("#addToCart").on('submit', function (e) {
                e.preventDefault();

                let qty = $('#quantity').val();
                let variant = $("input[name='variant']:checked").val();

                $("#qty_value").val(qty);

                $("#varaints_value").val(variant);

                if ($('input[type=radio]').length == 0) {


                    if(stockQty >= qty) {
                        $.ajax({
                            type: $(this).attr('method'),
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            success: function(response){
                                console.log(response);
                                Swal.fire(
                                    'Added to Cart!',
                                    'Thanks!',
                                    'success'
                                )
                            },
                            error: function(jqXHR, exception){

                                if(jqXHR.status == 401) {
                                    window.location.href = '/login';
                                }

                            }
                        })

                    }else {
                        $("#cartErr").html("Sorry, We don't have this much quantity in stock").show().fadeOut(5000);
                        return false;
                    }


                    // return true;


                } else {

                    if ($('input[type=radio]').is(':checked')) {

                        if(stockQty >= qty) {
                            $.ajax({
                                type: $(this).attr('method'),
                                url: $(this).attr('action'),
                                data: $(this).serialize(),
                                success: function(response){

                                    console.log(response);
                                    Swal.fire(
                                        'Added to Cart!',
                                        'Thanks!',
                                        'success'
                                    )
                                },
                                error: function(jqXHR, exception){
                                    if(jqXHR.status == 401) {
                                        window.location.href = '/login';
                                    }
                                }
                            })

                            return true;

                        }else {
                            $("#cartErr").html("Sorry, We don't have this much quantity in stock").show().fadeOut(5000);
                            return false;
                        }


                    }else {
                        $("#cartErr").html('PLease select the variant !').show().fadeOut(5000);
                        return false;
                    }

                }




            });


            $("#likeBtn").click(function (e) {

                e.preventDefault();

                let productId = $(this).attr('data-id');

                console.log(productId);
                $.ajax({
                    type: "GET",
                    data: {productId},
                    url : "{{url('wishlist/store')}}",
                    success: function (response) {

                        let jsonResponse = JSON.parse(response);

                        console.log(jsonResponse.status);

                        if(jsonResponse.status == true)
                        {

                            Swal.fire({
                                text : "It's Added to your wishlist!",
                                title: 'Yayy!',
                                type : 'success'
                            })

                            let likeBtn = $(".heart").css("background-color", "deeppink").show(500);

                            // console.log(likeBtn);
                            // likeBtn.hide(1500).show(1500);
                            // likeBtn.queue(function() {
                            //     likeBtn.css("background-color", "deeppink");
                            // });
                        }
                        else{

                            Swal.fire({
                                type : "info",
                                title: "Awww Snap!",
                                text : jsonResponse.message
                            })
                        }
                    },
                    error: function(jqXHR, exception){
                        if(jqXHR.status == 401) {
                            window.location.href = '/login';
                        }
                    }
                });
            })


        })
    </script>
@endsection
