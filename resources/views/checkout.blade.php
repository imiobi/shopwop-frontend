@extends('layouts.master')

@section('content')

<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
            <div class="col-first">
                <h1>Product Checkout</h1>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->
<!-- Start Checkout Area -->
<div class="container">
    <div class="checkput-login">


        <div class="collapse" id="checkout-login">
            <div class="checkout-login-collapse d-flex flex-column">
                <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                    <div class="row">
                        <div class="col-lg-4">
                            <input type="text" placeholder="Username or Email*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Username or Email*'" required class="common-input mt-10"  />

                        </div>
                        <div class="col-lg-4">
                            <input type="password" placeholder="Password*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password*'" required class="common-input mt-10">
                        </div>
                    </div>
                    <div class="d-flex align-items-center flex-wrap">
                        <button class="view-btn color-2 mt-20 mr-20"><span>Login</span></button>
                        <div class="mt-20">
                            <input type="checkbox" class="pixel-checkbox" id="login-1">
                            <label for="login-1">Remember me</label>
                        </div>
                    </div>
                <a href="#" class="mt-10">Lost your password?</a>
            </div>
        </div>
    </div>
    <div class="checkput-login mt-20">

        <div class="collapse" id="checkout-cupon">
            <div class="checkout-login-collapse d-flex flex-column">
                {{-- <form action="#" class="d-block">
                    <div class="row">
                        <div class="col-lg-8">
                            <input type="text" placeholder="Enter coupon code" onfocus="this.placeholder=''" onblur="this.placeholder = 'Enter coupon code'" required class="common-input mt-10">
                        </div>
                    </div>
                    <button class="view-btn color-2 mt-20"><span>Apply Coupon</span></button>
                </form> --}}
            </div>
        </div>
    </div>
</div>
<!-- End Checkout Area -->
<!-- Start Billing Details Form -->
<div class="container">
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            <h2>
                {{session()->get('success')}}
            </h2>
        </div>

    @endif

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{url('orders/create-order')}}" method="POST" class="billing-form">
        @csrf
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <h3 class="billing-title mt-20 mb-10">Billing Details</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <label for="first_name">First Name</label>
                        <input type="text" placeholder="First name*" id="first_name" onfocus="this.placeholder=''" onblur="this.placeholder = 'First name*'" value="{{$aCustomer->first_name}}" name="first_name" required class="common-input">
                    </div>
                    <div class="col-lg-6">
                        <label for="last_name">Last Name</label>
                        <input type="text" placeholder="Last name*" id="last_name" onfocus="this.placeholder=''" onblur="this.placeholder = 'Last name*'" value="{{$aCustomer->last_name}}" name="last_name" required class="common-input">
                    </div>

                    <div class="col-lg-6">
                        <label for="contact_no">Phone Number</label>

                        <input type="text" placeholder="Phone number*" id="contact_no" onfocus="this.placeholder=''" onblur="this.placeholder = 'Phone number*'" value="{{$aCustomer->contact_no}}" name="contact_no" required class="common-input">
                    </div>
                    <div class="col-lg-6">
                        <label for="email">Email</label>

                        <input type="email" placeholder="Email Address*" id="email" onfocus="this.placeholder=''" onblur="this.placeholder = 'Email Address*'" value="{{$aCustomer->email}}" name="email" required class="common-input" disabled>
                    </div>
                    <div class="col-lg-6">
                        <label>State/Province</label>
                        <div class="sorting">
                            <select name="state">
                                @foreach($aStates as $state)
                                    @php($isSelected = ($state == $aCustomer->$state) ? "selected" : "")
                                <option value="{{$state}}" {{$isSelected}}>{{$state}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label>Postal Code / ZIP</label>

                        <input type="text" placeholder="Postal code / ZIP" onfocus="this.placeholder=''" onblur="this.placeholder = 'Postcode/ZIP'" value="{{$aCustomer->postal_code}}" name="postal_code" required class="common-input">
                    </div>
                    <div class="col-lg-12">
                        <label>Address</label>

                        <input type="text" placeholder="Address*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Address line 01*'" value="{{$aCustomer->address}}" name="address" required class="common-input">
                    </div>

                    <div class="col-lg-12">
                        <label>Town / City</label>
                        <input type="text" placeholder="Town/City*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Town/City*'" value="{{$aCustomer->city}}" name="city" required class="common-input">
                    </div>



                    <div class="col-lg-12">
                        <label>Comments</label>
                        <textarea placeholder="Any extra detail you want to add?" onfocus="this.placeholder=''" onblur="this.placeholder = 'Any extra detail you want to add?'" name="comments" class="common-textarea"></textarea>
                    </div>
                </div>

                <h3 class="billing-title mt-20 mb-10">Shipping Address</h3>
                <div class="mt-20">
                    <input type="checkbox" class="pixel-checkbox" id="shipping_address">
                    <label for="login-6">Ship to a different address?</label>
                </div>
                <input type="text" placeholder="Order Notes" onfocus="this.placeholder=''" onblur="this.placeholder = 'Order Notes'" value="{{$aCustomer->shipped_address}}" name="shipped_address" id="shipped_address" required class="common-input" disabled>
            </div>
        
            <div class="col-lg-4 col-md-6">
                <div class="order-wrapper mt-50">
                    <h3 class="billing-title mb-10">Your Order</h3>
                    <div class="order-list">
                        <div class="list-row d-flex justify-content-between">
                            <div>Product</div>
                            <div>Total</div>
                        </div>
                        @foreach($aOrders as $order)
                        <div class="list-row d-flex justify-content-between">
                            <div>{{$order->cart_product->product_title}}</div>
                            <div>x {{$order->quantity_ordered}}</div>
                            <div>${{number_format($order->quantity_ordered * $order->cart_product->price)}}</div>
                        </div>
                        @endforeach

                        <div class="list-row d-flex justify-content-between">
                            <h6>Subtotal</h6>
                            <div>${{number_format($iGrandTotal)}}</div>
                        </div>
                        <div class="list-row d-flex justify-content-between">
                            <h6>Shipping</h6>
                            <div>Flat rate: $50.00</div>
                        </div>
                        <div class="list-row d-flex justify-content-between">
                            <h6>Total</h6>
                            <div class="total">${{number_format($iGrandTotal + 50)}}</div>
                        </div>

                        <div class="mt-20 d-flex align-items-start">
                            <input type="checkbox" class="pixel-checkbox" id="terms">
                            <label for="login-4">I’ve read and accept the <a href="#" class="terms-link">terms & conditions*</a></label>

                        </div>
                        <div style="color: red"><label id="termsErr"></label></div>
                        @if(isset($aOrders) && !empty($aOrders))
                        <button id="checkout" type="submit" class="view-btn color-2 w-100 mt-20"><span>Proceed to Checkout</span></button>
                        @endif
                    </div>
    </form>
                </div>
            </div>
        </div>
</div>
<!-- End Billing Details Form -->



@endsection


@section('footer')

    <script>
        $(function () {

            $("#shipping_address").click(function () {

                if($(this).prop('checked')){
                    $("#shipped_address").attr('disabled', false)
                }else {
                    $("#shipped_address").attr('disabled', true)

                }
            })



            $("#checkout").click(function () {

                if($("#terms").prop('checked') == false){

                    $("#termsErr").html('Please Read me!').show().fadeOut(3000);
                    return false;
                }

                return true
            })


        })
    </script>

@endsection

