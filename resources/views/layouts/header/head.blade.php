
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    @yield('title')

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">


    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="{{asset('/css/Linear-icons/linearicons.css')}}">
    <link rel="stylesheet" href="/css/Font-awesome/Font-awesome-4.7.0/font-awesome.min.css">
    <link rel="stylesheet" href="/css/Nice-select/nice-select.css">
    <link rel="stylesheet" href="/css/Ion-range-slider/Ion-range-slider-2.0.3/ion.rangeSlider.css" />
    <link rel="stylesheet" href="/css/Ion-range-slider/Ion-range-slider-2.0.3//ion.rangeSlider.skinFlat.css" />
    <link rel="stylesheet" href="/css/Bootstrap/Bootstrap-4.0.0/bootstrap.css">
    <link rel="stylesheet" href="/css/Main/main.css">
    <link rel="stylesheet" href="/css/style/full.css">
