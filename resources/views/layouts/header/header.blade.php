
<!-- Start Header Area -->
<header class="default-header">
    <div class="menutop-wrap">
        <div class="menu-top container">
            <div class="d-flex justify-content-between align-items-center">
                <ul class="list">
                    <li><!--a href="tel:+12312-3-1209">+12312-3-1209</a--></li>
                    <li><!--a href="mailto:support@colorlib.com">support@colorlib.com</a--></li>
                </ul>
                @if(!isset(Auth::user()->id))
                <ul class="list">
                    <li><a href="{{route('login')}}">login</a></li>
                </ul>
                @endif
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg  navbar-light">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{asset('storage/logo/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li><a href="/">Home</a></li>
                    <li><a href="/products?page=1&perPage=12">Products</a></li>
                    <li><a href="/cart">Cart</a></li>
                    <li><a href="/wishlist">Wish List</a></li>
                    <!-- Dropdown -->
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle" href="#" id="categories" data-toggle="dropdown">
                            Categories
                        </a>
                        <div class="dropdown-menu" id="categoryDropdown">

{{--                            <a class="dropdown-item" href="single.html">Single</a>--}}
{{--                            <a class="dropdown-item" href="cart.html">Cart</a>--}}
{{--                            <a class="dropdown-item" href="checkout.html">Checkout</a>--}}
{{--                            <a class="dropdown-item" href="confermation.html">Confermation</a>--}}
{{--                            <a class="dropdown-item" href="login.html">Login</a>--}}
{{--                            <a class="dropdown-item" href="tracking.html">Tracking</a>--}}
{{--                            <a class="dropdown-item" href="generic.html">Generic</a>--}}
{{--                            <a class="dropdown-item" href="elements.html">Elements</a>--}}
                        </div>
                    </li> -->
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle" href="#" id="brands" data-toggle="dropdown">
                            Brands
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">dummy link</a>
                        </div>
                    </li> -->
                    @if(Auth::check())
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" id="userDropDown" data-toggle="dropdown">
                            {{ Auth::user()->first_name}}
                        </a>
                        <div class="dropdown-menu">
                            <ul>
                                <li>
                                    <a class="view-btn color-2" href="{{url('users/my-orders')}}">My Orders</a>
                                </li>
                                <li>
                                    <form action="{{route("logout")}}" method="POST">
                                        @csrf
                                        <button class="view-btn color-2" type="submit" >{{ __('Logout') }}</button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                        @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End Header Area -->
