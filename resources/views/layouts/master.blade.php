<html>
<head>
    @include('layouts.header.head')
    @yield('head')
</head>
<body>

@yield('customeCSS')

@include('layouts.header.header')


@yield('content')


@include('layouts.footer.footer')

</body>
</html>





