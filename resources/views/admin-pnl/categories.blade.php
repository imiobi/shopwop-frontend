@extends('admin-pnl.layouts.admin')

@section('head')
    <link rel="stylesheet" href="{{asset('admin-assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection



@section('content')


    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="box-title pull-left">Categories</h3>
                        <a href="/admin-cms/categories/add-category" class="btn btn-primary pull-right">Add <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="box-body">

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif

                    @if(session()->has('errors'))
                        {{--                    {{dd(session()->get('errors'))}}--}}

                        <div class="alert alert-danger">
                            <ul>
                                @foreach(session()->get('errors') as $key => $error)
                                    <li>{{ $error[0] }}</li>
                                @endforeach

                            </ul>
                        </div>
                    @endif

                <div class="table-responsive">
                    <table id="CategoryTable" class="table table-dark">
                        <thead>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Active</th>
                        <th>Created At</th>
                        <th>Action</th>
                        </thead>

                        <tbody>
                        @if($aCategories != false)
                            @foreach ($aCategories as $oCategory)
                                <tr>
                                    <td>{{$oCategory->category_name}}</td>
                                    <td>{{$oCategory->description}}</td>
                                    <td>{{($oCategory->is_active) == 1 ? "Active" : "In-active"}}</td>
                                    <td>{{$oCategory->created_at}}</td>
                                    <td>

                                        <a href=categories/add-category/"{{encrypt($oCategory->id)}}" id="edit" class="btn btn-warning"><i
                                                    class="fa fa-edit"></i></a>

                                    
                                        <form style="display: inline-block" method="POST" action="categories/{{encrypt($oCategory->id)}}">
                                            {{csrf_field()}}{{method_field('DELETE')}}
                                            <button class="btn btn-danger"><i
                                                        class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                            <tr>
                                <td>No data Found!</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>


@endsection

@section('footer')

    <script src="{{asset('admin-assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


    <script>
        $("#CategoryTable").DataTable()
    </script>

@endsection