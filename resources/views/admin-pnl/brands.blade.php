@extends('admin-pnl.layouts.admin')

@section('head')
    <link rel="stylesheet" href="{{asset('admin-assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection




@section('content')


    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="box-title pull-left">Brands</h3>
                        <a href="/admin-cms/brands/add-brands" class="btn btn-primary pull-right">Add <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif

                    @if(session()->has('errors'))
                        <div class="alert alert-danger">
                            <ul>
                                @foreach(session()->get('errors') as $key => $error)
                                    <li>{{ $error[0] }}</li>
                                @endforeach

                            </ul>
                        </div>
                    @endif
                <div class="table-responsive">
                    <table id="brandsTable" class="table table-dark">
                        <thead>
                        <th>Brand Name</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Created At</th>
                        <th>Action</th>

                        </thead>

                        <tbody>
                        @if(count($aBrands) > 0)

                            @foreach ($aBrands as $oBrand)
                                <tr>
                                    <td>{{$oBrand->brand_name}}</td>
                                    <td>{{$oBrand->contact_no}}</td>
                                    <td>{{$oBrand->email}}</td>
                                    <td>{{$oBrand->country}}</td>
                                    <td>{{$oBrand->created_at ?? "N/A"}}</td>
                                    <td>

                                        <a href="/admin-cms/brands/get-brand/{{encrypt($oBrand->id)}}" id="edit" class="btn btn-warning"><i
                                                    class="fa fa-edit"></i></a>


                                        <form style="display: inline-block" method="POST" action="brands/{{encrypt($oBrand->id)}}">
                                            {{csrf_field()}}{{method_field('DELETE')}}
                                            <button id="delete" class="btn btn-danger"><i
                                                        class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td>No Data Found</td>
                                </tr>
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>



@endsection


@section('footer')

    <script src="{{asset('admin-assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


    <script>
        $("#brandsTable").DataTable()
    </script>

@endsection