@extends('admin-pnl.layouts.admin')


@section('header')
    <h1>
        {{ isset($oCategoryData) ? 'Update' : 'Add' }} Category
    </h1>

@endsection


@section('content')

    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">  {{ isset($oCategoryData) ? 'Update' : 'Add' }} Category</h3>
            </div>
            <div class="box-body">

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>

                @endif


                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif


                @php($action = (isset($oCategoryData)) ? "/admin-cms/categories/".encrypt($oCategoryData->id) : "/admin-cms/categories")


                <form method="POST" action="{{$action}}"  enctype="multipart/form-data">

                    @if(isset($oCategoryData))
                        {{method_field('PUT')}}
                    @endif
                    {{csrf_field()}}
                    <div class="box-body">

                        <div class="form-group col-md-6">
                            <label for="category_name">Category Name</label>
                            <input type="text" name="category_name" class="form-control" id="category_name" value="{{isset($oCategoryData) ? $oCategoryData->category_name : ""}}" placeholder="Category Name">
                        </div>


                        <!-- select -->
                        <div class="form-group col-md-6">
                            <label>Status</label>
                            <select name="is_active" class="form-control">
                                <option value="">::SELECT STATUS::</option>
                                <option value="1" {{ isset($oCategoryData->is_active) && ($oCategoryData->is_active == 1) ? 'selected': '' }}>Active</option>
                                <option value="0" {{ isset($oCategoryData->is_active) && ($oCategoryData->is_active == 0) ? 'selected': '' }}>In-Active</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea type="number" name="description" class="form-control" rows="5" col="55" id="description" placeholder="Description">{{isset($oCategoryData) ? $oCategoryData->description : ""}}</textarea>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input id="isParent" type="checkbox" {{isset($oCategoryData->parent_category_id) ? 'checked' : ''}}>
                                    Has Parent Category ?
                                </label>
                            </div>
                        </div>

                        <!-- select -->
                        <div class="form-group col-md-6">
                            <label>Select Parent Category</label>
                            <select name="parent_category_id" id="categories" class="form-control" disabled>

                                @if(isset($aCategories) && count($aCategories) >= 1)
                                    <option value=""> ::SELECT PARENT CATEGORY::</option>

                                    @foreach($aCategories as $category )
                                        @if(isset($oCategoryData))
                                            @php($isSelected = ($category->id == $oCategoryData->id) ? 'selected' : '' )
                                        @endif

                                    @if($category->parent_category_id == null)
                                          <option value="{{$category->id}}" {{@$isSelected}} >{{$category->category_name}}</option>
                                    @endif
                                    @endforeach
                                @else
                                    <option value=""> No Category Found</option>
                                @endif
                            </select>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.box -->
    </div>


@endsection

@section('footer')
    <script>
        $(function () {

            if($('#isParent').is(':checked')) {
                $("#categories").attr("disabled", false)
            }

            $('#isParent').click(function() {
                if ($(this).is(':checked') == true) {
                    $("#categories").attr("disabled", false)
                }else {
                    $("#categories").attr("disabled", true)
                }
            });
        })
    </script>
@endsection