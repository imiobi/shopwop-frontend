@extends('admin-pnl.layouts.admin')

@section('head')
    <link rel="stylesheet" href="{{asset('admin-assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
            <h3 class="box-title"><b> {{ Request::segment(3) ? ucwords(Request::segment(3)) : ucwords(Request::segment(2))}} Orders </b></h3>
            </div>
            <div class="box-body">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
                <div class="table-responsive">
                    <table id="orderTable" class="table table-dark">
                        <thead>
                        <th>Order Number</th>
                        <th>Status</th>
                        <th>Required Date</th>
                        <th>Shipped Date</th>
                        <th>Comments</th>
                        <th>Created At</th>
                        <th>Action</th>
                        </thead>

                        <tbody>
                        @foreach ($oOrders as $oOrder)
                            <tr>
                                <td>{{$oOrder->order_number}}</td>
                                <td>{{$oOrder->status}}</td>
                                <td>{{$oOrder->required_date}}</td>
                                <td>{{isset($oOrder->shipped_date) && !empty($oOrder->shipped_date) ? $oOrder->shipped_date : "N/A"}}</td>
                                <td>{{isset($oOrder->comments) && !empty($oOrder->comments) ? $oOrder->comments: "N/A"}}</td>
                                <td>{{$oOrder->created_at}}</td>
                                <td>

                                    <a href="/admin-cms/orders/edit/{{encrypt($oOrder->id)}}" id="edit" class="btn btn-warning">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="box-footer">

            </div>
        </div>
    </div>
@endsection


@section('footer')


    <script src="{{asset('admin-assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(function () {
            $('#orderTable').DataTable()
        })
    </script>

@endsection

