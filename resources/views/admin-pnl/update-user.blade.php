@extends('admin-pnl.layouts.admin')
{{-- {{dd($aUser->address)}} --}}
@section('header')
    <h1>
        Update User
    </h1>

@endsection


@section('content')

    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Update User</h3>
            </div>
            <div class="box-body">

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>

                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif



                <form method="POST" action="{{url('admin-cms/users/update/'. encrypt($aUser->id) )}}">
                    {{method_field('PUT')}}
                    {{csrf_field()}}

                    <div class="box-body">

                        <div class="form-group col-md-6">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" class="form-control" id="first_name" value="{{$aUser->first_name}}" placeholder="First Name">
                         </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" class="form-control" id="last_name" value="{{$aUser->last_name}}" placeholder="Last Name">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" value="{{$aUser->email}}" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_no">Contact No</label>
                            <input type="number" name="contact_no" class="form-control" id="contact_no" value="{{$aUser->contact_no}}" placeholder="Contact No">
                        </div>
                        <div class="form-group col-md-6">
                            <label>USER TYPE</label>
                            <select name="is_admin" class="form-control">
                                <option value="">Select Type</option>
                                <option value="1" {{ $aUser->is_admin == 1 ? 'selected': ''}}>IS ADMIN</option>
                                <option value="0" {{ $aUser->is_admin == 0 ? 'selected': ''}}>USER</option>
                            </select>
                        </div>
                   
                        <div class="form-group col-md-6">
                            <label for="company">Company</label>
                            <input type="text" name="company" class="form-control" id="company" value="{{$aUser->company}}" placeholder="Company">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date_of_birth">Date Of Birth</label>
                            <input type="date" name="date_of_birth" class="form-control" id="date_of_birth" value="{{$aUser->date_of_birth}}" placeholder="Date of Birth">
                        </div>

                      

                        <div class="form-group col-md-6">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control" id="city" value="{{$aUser->city}}" placeholder="City">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="state">State</label>
                            <input type="text" name="state" class="form-control" id="state" value="{{$aUser->state}}" placeholder="State">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="postal_code">Postal Code</label>
                            <input type="number" name="postal_code" class="form-control" id="postal_code" value="{{$aUser->postal_code}}" placeholder="Postal Code">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address">Address</label>
                            <input type="text" name="address" class="form-control" id="address" value="{{$aUser->address}}" placeholder="Address">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="shipped_address">Shipped Address</label>
                            <input type="text" name="shipped_address" class="form-control" id="shipped_address" value="{{$aUser->shipped_address}}" placeholder="Shipped Address">
                        </div>

                        <div class="form-group col-md-12">
                            <input type="submit" class="btn btn-primary btn-sm pull-right" value="Update" />
                        </div>
                </div>

                </form>

                    
            </div>
        </div>
    </div>

@endsection


@section('footer')
    <!-- CK Editor -->
    <script src="{{asset('admin-assets/bower_components/ckeditor/ckeditor.js')}}"></script>
@endsection