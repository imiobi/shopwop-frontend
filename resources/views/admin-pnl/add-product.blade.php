@extends('admin-pnl.layouts.admin')


@section('header')
    <h1>
        Add Product
    </h1>

@endsection


@section('content')

    <div class="col-md-12">
        @php($action = (isset($oProduct)) ? "/admin-cms/products/".encrypt($oProduct->id) : "/admin-cms/products")
        <form method="POST" action="{{$action}}" id="add_product_form"  enctype="multipart/form-data">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Product</h3>
            </div>
            <div class="box-body">

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>

                @endif

                @if(session()->has('Success'))
                    <div class="alert alert-success">
                        {{session()->get('Success')}}
                    </div>
                @endif

                {{--@if(session()->has('errors'))
                    {{dd(session()->get('errors'), $errors->all())}}

                    <div class="alert alert-danger">
                        <ul>
                                @foreach(session()->get('errors') as $key => $error)
                                    <li>{{ $error[0] }}</li>
                                @endforeach

                        </ul>
                    </div>
                @endif--}}




{{--                    @if(isset($oProduct))--}}
{{--                        {{dd($oProduct)}}--}}
{{--                    @endif--}}

                    @if(isset($oProduct))
                        {{method_field('PUT')}}
                    @endif
                    {{csrf_field()}}
                    <div class="box-body">

                    {{-- <div class="col-md-6"> --}}
                         <div class="form-group col-md-6">
                            <label for="product_title">Product Title</label>
                            <input type="text" name="product_title" class="form-control" id="product_title" value="{{isset($oProduct) ? $oProduct->product_title : ""}}" placeholder="Produc Title">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="stock_quantity">Stock Quantity</label>
                            <input type="number" name="stock_quantity" class="form-control" id="stock_quantity" value="{{isset($oProduct) ? $oProduct->stock_quantity : ""}}" placeholder="Stock Quantity">
                        </div>
                    {{-- </div> --}}

                    {{-- <div class="col-md-6"> --}}
                         <div class="form-group col-md-6">
                            <label for="price">Price</label>
                            <input type="number" name="price" class="form-control" id="price" value="{{isset($oProduct) ? $oProduct->price : ""}}" placeholder="Price">
                        </div>
                         <div class="form-group col-md-6">
                            <label for="variants">Variants</label>
                            <input type="text" name="variants" class="form-control" id="variants" value="{{isset($oProduct) ? $oProduct->variants : ""}}" placeholder="Variants">
                        </div>
                    {{-- </div> --}}

                        <div class="form-group col-md-6">
                            <label for="product_description">Product Desctiption</label>
                            <textarea name="product_description" class="form-control" rows="5" col="55" id="product_description" placeholder="Product Description">{{isset($oProduct) ? $oProduct->product_description : ""}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="short_description">Short Desctiption</label>
                            <textarea name="short_description" class="form-control" rows="5" col="55" id="short_description" placeholder="Short Description">{{isset($oProduct) ? $oProduct->short_description : ""}}</textarea>
                        </div>


                        <!-- select -->
                        <div class="form-group col-md-6">
                            <label>Brands</label>

                            <select name="brand_id" class="form-control">

                                @foreach($brands as $brand )

                                    @if(isset($oProduct))
                                        @php(
                                            $isSelected = ($brand->id == $oProduct->brand_id) ? 'selected' : ''
                                        )
                                    @endif

                                    <option value="{{$brand->id}}" {{@$isSelected}}> {{ $brand->brand_name }}</option>

                                @endforeach
                            </select>
                        </div>

                        <!-- select -->
                        <div class="form-group col-md-6">
                            <label>Categories</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $category )

                                    @if(isset($oProduct))
                                        @php(
                                            $isSelected = ($category->id == $oProduct->category_id) ? 'selected' : ''
                                        )
                                    @endif


                                    <option value="{{$category->id}}" {{@$isSelected}} >{{$category->category_name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <!-- select -->
                        <div class="form-group col-md-6">
                            <label>Active Product</label>
                            <select name="is_active" class="form-control">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>


                        <div class="form-group col-md-2">
                            <label for="image">File input</label>
                            <input type="file" name="image" id="image">
                            @if(isset($oProduct))
                                <img src="{{ isset($oProduct->image[0]) ? asset('storage/thumbnail/'.$oProduct->image[0]->image_name) : ''}}">
                            @endif
                        </div>
                        @if($errors->has('image'))
                            <div class="alert alert-danger">
                                <ul>
                                    {{$errors->messages()['image'][0]}}

                                </ul>
                            </div>
                        @endif
                    </div>
                    <!-- /.box-body -->


            </div>

        </div>
        <!-- /.box -->

        <div class="box">

            <div class="box-body">
                <div class="box-body pad">

                    <textarea id="editor1" name="specifications" rows="10" cols="80">
                                       {{isset($oProduct) ? $oProduct->specifications : ""}}
                    </textarea>


                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>


        </div>
        </form>

    </div>


@endsection


@section('footer')
    <!-- CK Editor -->
    <script src="{{asset('admin-assets/bower_components/ckeditor/ckeditor.js')}}"></script>

    <script>
        CKEDITOR.replace( 'editor1' );
        config.extraPlugins = 'preview';
    </script>
@endsection
