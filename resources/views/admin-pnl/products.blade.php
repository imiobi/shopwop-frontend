@extends('admin-pnl.layouts.admin')

@section('head')
    <link rel="stylesheet" href="{{asset('admin-assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')


    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">


                    <div class="col-md-12">
                        <h3 class="box-title pull-left">Products</h3>
                        <a href="/admin-cms/products/add-product" class="btn btn-primary pull-right">Add <i
                                    class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="box-body">

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
                <div class="table-responsive">
                    <table id="productTable" class="table table-dark">
                        <thead>
                        <th>#</th>
                        <th>Code</th>
                        <th>Title</th>
                        <th>Stock Quantity</th>
                        <th>Price</th>
                        <th>Views</th>
                        <th>Active</th>
                        <th>Brand Name</th>
                        <th>Category Name</th>
                        <th>Created At</th>
                        <th>Action</th>
                        </thead>

                        <tbody>
                        @foreach ($oProducts as $oProduct)
                            <tr>
                                <td>{{$oProduct->id}}</td>

                                <td>{{$oProduct->product_code}}</td>
                                <td>{{$oProduct->product_title}}</td>
                                <td>{{$oProduct->stock_quantity}}</td>
                                <td>{{$oProduct->price}}</td>
                                <td>{{$oProduct->view_count}}</td>
                                <td>{{($oProduct->is_active) === 1 ? "Active" : "In-active"}}</td>
                                <td>{{$oProduct->brand_name}}</td>
                                <td>{{$oProduct->category_name}}</td>
                                <td>{{$oProduct->created_at}}</td>
                                <td>

                                    <a href="/admin-cms/products/get-product/{{encrypt($oProduct->id)}}" id="edit" class="btn btn-warning"><i
                                                class="fa fa-edit"></i></a>
                                    <form style="display: inline-block" method="POST" action="products/{{encrypt($oProduct->id)}}">
                                        {{csrf_field()}}{{method_field('DELETE')}}
                                        <button data-id="{{$oProduct->id}}" id="delete" class="btn btn-danger"><i
                                                    class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>
@endsection

@section('footer')

    {{-- bootstrap data-table --}}
    <script src="{{asset('admin-assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(function () {
            $('#productTable').DataTable()
        })
    </script>


@endsection