@extends('admin-pnl.layouts.admin')


@section('header')
    <h1>
        Add Brands
    </h1>

@endsection


@section('content')

    <div class="col-md-12">
        <div class="box">
            
            <div class="box-header with-border">
                <h3 class="box-title">Add Brands</h3>
            </div>

            <div class="box-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>

            @endif


            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif

                @php($action = (isset($oBrandData)) ? "/admin-cms/brands/".encrypt($oBrandData->id) : "/admin-cms/brands")


                <form method="POST" action="{{$action}}"  enctype="multipart/form-data">

                    @if(isset($oBrandData))
                        {{method_field('PUT')}}
                    @endif
                    {{csrf_field()}}
                    <div class="box-body">

                        <div class="form-group col-md-6">
                            <label for="brand_name">Brand Name</label>
                            <input type="text" name="brand_name" class="form-control" id="brand_name" value="{{isset($oBrandData) ? $oBrandData->brand_name : ""}}" placeholder="Brand Name">
                        </div>



                        <div class="form-group col-md-6">
                            <label for="contact_no">Contact Number</label>
                            <input type="text" name="contact_no" class="form-control" id="contact_no" value="{{isset($oBrandData) ? $oBrandData->contact_no : ""}}" placeholder="Contact Number"></input>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" value="{{isset($oBrandData) ? $oBrandData->email : ""}}" placeholder="Email"></input>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="country">Country</label>
{{--                            <input type="country" name="country" class="form-control" id="country" placeholder="Country">{{isset($oBrandData) ? $oBrandData->country : ""}}</input>--}}
                            <select id="country" name="country" class="form-control">

                            </select>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.box -->
    </div>


@endsection


@section('footer')

    <script>
        $(function () {
            $.ajax({
                url: "https://restcountries.eu/rest/v2/all",
                dataType: "json",
                success: function (data) {
                    console.log(data);

                    $.each(data, function (index, item) {
                        $('#country').append($('<option>', {value:item.name, text:item.name}));

                    })
                }
            });
        })
    </script>
@endsection