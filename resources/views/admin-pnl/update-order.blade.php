@extends('admin-pnl.layouts.admin')

@section('header')
    <h1>
        Update Order
    </h1>

@endsection


@section('content')

    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Update Order</h3>
            </div>
            <div class="box-body">

                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>

                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{session()->get('success')}}
                    </div>
                @endif



                <form method="POST" action="{{url('admin-cms/orders/update/'. encrypt($aOrder->id) )}}">
                    {{method_field('PUT')}}
                    {{csrf_field()}}

                    <div class="box-body">

                        <div class="form-group col-md-6">
                            <label for="order_number">Order Number</label>
                            <input type="text" name="order_number" class="form-control" id="order_number" value="{{$aOrder->order_number}}" placeholder="First Name" Disabled>
                         </div>
                       
                         <div class="form-group col-md-6">
                            <label>Status</label>
                            <select name="status" class="form-control">
                                <option value="">::Select Status::</option>
                                @if(isset($aStatuses) && count($aStatuses) > 0)
                                    @foreach($aStatuses as $status)
                                        <option value="{{$status}}" {{$status == $aOrder->status ? 'selected' : ''}}>{{$status}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="customer_name">Customer Name</label>
                            <input type="text" name="customer_name" class="form-control" id="customer_name" value="{{$aOrder->customer_name}}" Disabled>
                        </div>


                        <input type="hidden" name="customer_id" value="{{$aOrder->customer_id}}" >


                        <div class="form-group col-md-6">
                            <label for="created_at">Created At</label>
                        <input type="datetime-local" name="created_at" class="form-control" id="created_at" value="{{ $aOrder->created_at }}" placeholder="">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="required_date">Required Date</label>
                            <input type="date" name="required_date" class="form-control" id="required_date" value="{{$aOrder->required_date}}" placeholder="Date of Birth">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="shipped_date">Shipped Date</label>
                            <input type="date" name="shipped_date" class="form-control" id="shipped_date" value="{{$aOrder->shipped_date}}" placeholder="Date of Birth">
                        </div>

                        <div class="form-group col-md-12" >
                            <label for="comments">Comments</label>
                            <textarea disabled name="comments" id="comments" class="form-control">
                                {{ $aOrder->comments }}
                            </textarea>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="total_amount">Payment Amount</label>
                            <input type="numeric" name="total_amount" class="form-control" id="total_amount" value="{{$aOrder->payment->total_amount}}" placeholder=" Total Amount">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Payment Status</label>
                            <select name="is_paid" class="form-control">
                                <option value="">::Select Payment Status::</option>
                                <option value="1" {{ $aOrder->payment->is_paid == 1 ? 'selected': ''}}>Paid</option>
                                <option value="0" {{ $aOrder->payment->is_paid == 0 ? 'selected': ''}}>Not Paid</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="payment_date">Payment Date</label>
                            <input type="datetime-local" name="payment_date" class="form-control" id="payment_date" value="{{$aOrder->payment->payment_date}}" placeholder="Payment Date">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Payment Method</label>
                            <input type="text"  class="form-control"  value="{{$aOrder->payment->payment_method_name}}" placeholder="Payment Method" disabled>
                        </div>

                        <div class="form-group col-md-12">
                            <input type="submit" class="btn btn-primary btn-sm pull-right" value="Update Order" />
                        </div>

                    </div> 
                </form>

                    
            </div>
        </div>
    </div>

@endsection


@section('footer')
    <!-- CK Editor -->
    <script src="{{asset('admin-assets/bower_components/ckeditor/ckeditor.js')}}"></script>
@endsection