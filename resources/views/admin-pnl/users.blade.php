@extends('admin-pnl.layouts.admin')

@section('head')
    <link rel="stylesheet" href="{{asset('admin-assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection



@section('content')

<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><b> All {{ ucwords(Request::segment(3)) === 'All' ? '' : ucwords(Request::segment(3)) }} Users </b></h3>
        </div>

        <div class="box-body">

            @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
            @endif @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
            @endif

            <div class="table-responsive">
                <table id="userTable" class="table table-dark">
                    <thead>
                        <th>Name</th>
                        <th>Email</th>

                        @if(Request::segment(3) == 'all')
                        <th>User Type</th>
                        @endif

                        <th>DOB</th>
                        <th>City</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </thead>

                    <tbody>
                        @foreach ($aUsers as $oUser)
                        <tr>
                            <td>{{$oUser->first_name . ' ' . $oUser->last_name}}</td>
                            <td>{{$oUser->email ?? 'N/A'}}</td>

                            @if(Request::segment(3) == 'all')
                            <td>{{ ($oUser->is_admin == 1) ? 'Admin' : 'Customer'}}</td>
                            @endif

                            <td>{{$oUser->date_of_birth ?? 'N/A'}}</td>
                            <td>{{$oUser->city ?? 'N/A'}}</td>
                            <td>{{$oUser->created_at ?? 'N/A'}}</td>

                            <td>
                                <a href="{{url('admin-cms/users/edit/'.encrypt($oUser->id))}}" id="edit" class="btn btn-warning">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <form style="display: inline-block" method="POST" action="{{url('admin-cms/users/delete/'.encrypt($oUser->id))}}">

                                    {{csrf_field()}} {{method_field('DELETE')}}

                                    <button type="submit" id="delete" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="box-footer">

        </div>
    </div>
</div>
@endsection


@section('footer')


    <script src="{{asset('admin-assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(function () {
            $('#userTable').DataTable()
        })
    </script>

@endsection