@extends('layouts.master')
{{--@extends('layouts.main')--}}

@section('content')

    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="container-fluid">
            <div class="row fullscreen align-items-center justify-content-center">
                <div class="col-lg-6 col-md-12 d-flex align-self-end img-right no-padding">
                    <img class="img-fluid" src="{{asset('storage/header/header-img.png')}}" alt="">
                </div>
                <div class="banner-content col-lg-6 col-md-12">
                    <h1 class="title-top"><span>Flat</span> 75%Off</h1>
                    <h1 class="text-uppercase">
                        It’s Happening <br>
                        this Season!

                    </h1>
                    <button class="primary-btn text-uppercase"><a href="products">Purchase Now</a></button>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start category Area -->
    <section class="category-area section-gap section-gap" id="catagory">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-40">
                    <div class="title text-center">
                        <h1 class="mb-10">Shop for Different Categories</h1>
                        <p>Who are in extremely love with eco friendly system.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 mb-10">
                    <div class="row category-bottom">
                        @for($i = 0; $i < count($aTopCategoryProducts) -1; $i++)
{{--                            {{dd()}}--}}
{{--                        @foreach($aTopCategoryProducts as $oTopCategory)--}}
                        <div class="col-lg-6 col-md-6 mb-30">
                            <div class="content">
                                <a href="#" target="_blank">
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto" src="{{asset('storage/home/c2.jpg')}}" alt="">
                                    <div class="content-details fadeIn-bottom">
                                        <h3 class="content-title">{{$aTopCategoryProducts[$i]->category_name}}</h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 mb-10">
                    <div class="content">
                        <a href="#" target="_blank">
                            <div class="content-overlay"></div>
                            <img class="content-image img-fluid d-block mx-auto" src="{{asset('storage/home/c4.jpg')}}" alt="">
                            <div class="content-details fadeIn-bottom">
                                <h3 class="content-title">{{end($aTopCategoryProducts)->category_name}}</h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start men-product Area -->
    <section class="men-product-area section-gap relative" id="men">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-40">
                    <div class="title text-center">
                        <h1 class="text-white mb-10">New realeased Products</h1>
                        <p class="text-white">Who are in extremely love with eco friendly system.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($aRecentProducts as $oRecentProduct)
{{--                    {{dd($oRecentProduct)}}--}}
                <div class="col-lg-3 col-md-6 single-product">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image img-fluid d-block mx-auto" src="{{asset('storage/img/'. $oRecentProduct->image[0]->image_name)}}" alt="">
                        <div class="content-details fadeIn-bottom">
                            <div class="bottom d-flex align-items-center justify-content-center">
                                <a class="wishList" data-id="{{$oRecentProduct->id}}"><span class="lnr lnr-heart"></span></a>
                                <a href="products/{{encrypt($oRecentProduct->id)}}"><span class="lnr lnr-frame-expand"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="price">
                        <h5 class="text-white">{{$oRecentProduct->product_title}}</h5>
                        <h3 class="text-white">${{$oRecentProduct->price}}</h3>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End men-product Area -->

    <!-- Start women-product Area -->
    <section class="women-product-area section-gap" id="women">
        <div class="container">
            <div class="countdown-content pb-40">
                <div class="title text-center">
                    <h1 class="mb-10">Top Sold Products</h1>
                    <p>Who are in extremely love with eco friendly system.</p>
                </div>
            </div>
            <div class="row">
                @foreach($aTopSoldProducts as $aTopSoldProduct)
{{--                    {{dd($aTopSoldProduct->image[0]->image_name)}}--}}
                <div class="col-lg-3 col-md-6 single-product">
                    <div class="content">
                        <div class="content-overlay"></div>
                        <img class="content-image img-fluid d-block mx-auto" src="storage/img/{{$aTopSoldProduct->image[0]->image_name}}" alt="">
                        <div class="content-details fadeIn-bottom">
                            <div class="bottom d-flex align-items-center justify-content-center">
                                <a class="wishList" data-id="{{$aTopSoldProduct->id}}"><span class="lnr lnr-heart"></span></a>
                                <a href="products/{{encrypt($aTopSoldProduct->id)}}"><span class="lnr lnr-frame-expand"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="price">
                        <h5>{{$aTopSoldProduct->product_title}}</h5>
                        <h3>${{$aTopSoldProduct->price}}</h3>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End women-product Area -->


    <!-- Start related-product Area -->
    <section class="related-product-area section-gap" id="latest">
        <div class="container">
            <div class="related-content">
                <div class="title text-center">
                    <h1 class="mb-10">Top Viewed Products</h1>
                    <p>Who are in extremely love with eco friendly system.</p>
                </div>
            </div>
            <div class="row">
                @foreach($aTopViewedProducts as $oTopViewedProduct)
                <div class="col-lg-3 col-md-4 col-sm-6 mb-20">
                    <div class="single-related-product d-flex">
                        <a href="products/{{encrypt($oTopViewedProduct->id)}}"><img src="{{asset('storage/thumbnail/'. $oTopViewedProduct->image[0]->image_name)}}" alt=""></a>
                        <div class="desc">
                            <a href="#" class="title">{{$oTopViewedProduct->product_title}}</a>
                            <div class="price"><span class="lnr lnr-tag"></span> ${{number_format($oTopViewedProduct->price)}}</div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
    </section>
    <!-- End related-product Area -->




@endsection

@section('footer')

    <script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

    <script>
        $(document).ready(function () {

            $(".wishList").click(function (e) {

                e.preventDefault();
                let productId = $(this).attr('data-id');

                $.ajax({
                    type: "GET",
                    data: {productId},
                    url : "wishlist/store",
                    success: function (response) {

                        let jsonResponse = JSON.parse(response);

                        console.log(jsonResponse.status);

                        if(jsonResponse.status == true)
                        {

                            Swal.fire({
                                text : "It's Added to your wishlist!",
                                title: 'Yayy!',
                                type : 'success'
                            })
                        }
                        else{

                            Swal.fire({
                                type : "info",
                                title: "Awww Snap!",
                                text : jsonResponse.message
                            })
                        }
                    },
                    error: function(jqXHR, exception){
                        if(jqXHR.status == 401) {
                            window.location.href = '/login';
                        }
                    }
                });
            })
        })
    </script>
@endsection

