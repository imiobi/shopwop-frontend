@extends('layouts.master')

@section('head')
    <link rel="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" href="sweetalert2.min.css">

@endsection


@section('content')


    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                <div class="col-first">
                    <h1>My Wishlist</h1>

                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->



    <!-- Start Cart Area -->
    <div class="container">
        <div class="cart-title">
            <div class="row">
                <div class="col-md-4">
                    <h6>Product Title</h6>
                </div>
                <div class="col-md-2">
                    <h6>Price</h6>
                </div>

                <div class="col-md-2">
                    <h6>Stock Status</h6>
                </div>
                <div class="col-md-2">
                    <h6>Add to Cart</h6>
                </div>
                <div class="col-md-2">
                    <h6>Remove</h6>
                </div>
            </div>
        </div>

        @if(isset($aProductData) && !empty($aProductData))
            @foreach($aProductData as $oWishList)
                <div class="cart-single-item">
                    <div class="row align-items-center">
                        <div class="col-md-4 col-12">
                            <div class="product-item d-flex align-items-center">
                                <img src="{{ isset($oWishList->data->image[0]->image_name) ? 'storage/thumbnail/'.$oWishList->data->image[0]->image_name: ''}} " class="img-fluid" alt="" />
                                <h6>{{$oWishList->data->product_title}}</h6>
                            </div>
                        </div>

                        <div class="col-md-2 col-6">
                            <div class="price">${{number_format($oWishList->data->price)}}</div>
                        </div>
                        <div class="col-md-2 col-6">
                            <div class="price"> <b>{{($oWishList->data->stock_quantity > 0 ) ? "In Stock" : "Out of Stock"}}</b></div>
                        </div>
                        <div class="col-md-2 col-6">
                            <div class=" d-flex align-items-center justify-content-between flex-wrap">
                                <a class="view-btn wishList-btn color-2" href="{{url('products/'.encrypt($oWishList->data->id))}}"><span>Add To Cart <i class="lnr lnr-cart"></i></span></a>
                            </div>
                        </div>
                        <div class="col-md-2 col-12 remove-cart">
                            <button type="button" data-id="{{$oWishList->data->id}}" class="removeCart view-btn color-1" ><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            @endforeach

            @else
            <div class="cart-single-item">
                <div class="row align-items-center">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <h5 style="color :deeppink">Your WishList is empty</h5>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


@section('footer')
    <script>
        $(document).ready(function () {
            $('.removeCart').click(function () {

                let product_id = $(this).attr('data-id');

                console.log(product_id);

                if(product_id) {

                    $.ajax({
                        type : "DELETE",
                        data : {
                            product_id,
                            "_token": "{{ csrf_token()}}"
                        },
                        url  : "wishlist/delete",
                        success: function (response) {

                            let jsonResponse = JSON.parse(response);

                            setTimeout(function () {

                                window.location.reload()
                            }, 200);
                        }

                    });
                }
            })
        })
    </script>
@endsection