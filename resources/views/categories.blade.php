@extends('layouts.master')

{{--@php--}}

{{--    $pages = ceil($oProducts->total/$oProducts->per_page);--}}

{{--@endphp--}}

@section('head')
    <link rel="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" href="sweetalert2.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <style>
        .btn-rounded {
            border-radius: 10em;
        }

        [type=reset], [type=submit], button, html [type=button] {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        .btn-mdb-color {
            background-color: #59698d!important;
            color: #fff;
        }

        .btn.btn-sm {
            padding: .5rem 1.6rem;
            font-size: .64rem;
        }
        [type=button]:not(:disabled), [type=reset]:not(:disabled), [type=submit]:not(:disabled), button:not(:disabled) {
            cursor: pointer;
        }

        .btn-mdb-color:not([disabled]):not(.disabled).active, .btn-mdb-color:not([disabled]):not(.disabled):active, .show>.btn-mdb-color.dropdown-toggle {
            box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            background-color: #323a4e!important;
        }
        .btn:not([disabled]):not(.disabled).active, .btn:not([disabled]):not(.disabled):active {
            box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
        }

    </style>
@endsection
@section('content')
    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                <div class="col-first">
                    <h1>{{$aCategoryProducts->category_name}}</h1>

                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar -->

                <form  method="POST" action="{{url('/products?page=1&perPage=12&sortBy=product_title&orderBy=asc')}}" class=" form-inline md-form mr-auto mb-4">
                    @csrf
                    <input class="form-control mr-lg" id="searchBox" type="text" name="q" placeholder="Search" aria-label="Search">
                    <button class="btn btn-mdb-color btn-rounded btn-sm my-0 waves-effect waves-light" type="submit">Search</button>
                </form>


                <div class="filter-bar d-flex flex-wrap align-items-center">

                    <div class="sorting">
                        <input type="hidden" name="category-id" value="{{$aCategoryProducts->id}}" />
                        <select class="sortBy">
                            <option value="">Select Sort By</option>
                            <option value="price" {{$sortBy == "price" ? 'selected' : ''}}>Price</option>
                            <option value="product_title" {{$sortBy == "product_title" ? 'selected' : ''}}>Product Title</option>
                        </select>
                    </div>
                    <div class="sorting">
                        <select class="orderBy">
                            <option value="asc" {{$orderBy == 'asc' ? 'selected' : ''}}>Ascending</option>
                            <option value="desc" {{$orderBy == 'desc' ? 'selected' : ''}}>Descending</option>
                        </select>

                    </div>
                </div>

                <!-- End Filter Bar -->
                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">
                    <div class="row">
                        @if(count($aCategoryProducts->products) > 0 )
                            @foreach($aCategoryProducts->products as $product)

                                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-6 single-product">
                                    <div class="content">
                                        <div class="content-overlay"></div>
                                        <img class="content-image img-fluid d-block mx-auto"
                                             src={{ isset($product->image[0]) ?  asset('storage/img/'.$product->image[0]->image_name) : ''}} alt=""/>
                                        <div class="content-details fadeIn-bottom">
                                            <div class="bottom d-flex align-items-center justify-content-center">
                                                <a class="wishList" data-id="{{$product->id}}"><span
                                                            class="lnr lnr-heart"></span></a>
                                                <a href="products/{{encrypt($product->id)}}"><span
                                                            class="lnr lnr-frame-expand"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price">
                                        <h5><a href="{{url('products/'.encrypt($product->id))}}"> {{ucwords($product->product_title)}} </a></h5>
                                        <h3>$ {{$product->price}}</h3>
                                    </div>
                                </div>
                            @endforeach

                        @else
                            <div class="col-xl-4 col-lg-6 col-md-12 col-sm-6 single-product">
                                <div class="content">
                                    <h3>No Product Found !</h3>
                                </div>
                            </div>
                        @endif
                    </div>
                </section>
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <div class="sorting mr-auto">

                        <select name="perPage" class="perPage">
                            <option value="">SELECT PER PAGE</option>
                            <option value="12" {{Request::get('perPage') == 12 ? 'selected' : ''}}>Show 12</option>
                            <option value="24" {{Request::get('perPage') == 24 ? 'selected' : ''}}>Show 24</option>
                            <option value="48" {{Request::get('perPage') == 48 ? 'selected' : ''}}>Show 48</option>
                        </select>
                    </div>

                    {{-- <div class="pagination">
                        @php($prevPage = ($oProducts->current_page > 1) ? $oProducts->current_page - 1 : 1 )
                        <a {{$prevPage == 1 ? 'disabled': ''}} href="{{url('/products?page=' . $prevPage)}}" class="prev-arrow"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>

                        @for($page = 1; $page <=$pages; $page++)
                            <a href="{{url('/products?page=' . $page)}}"  class="{{($pageNumber == $page) ? 'active' : '' }}"> {{ $page }} </a>
                        @endfor

                        {{-- <a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                        <a href="{{url('/products?page=' . $oProducts->last_page)}}"> {{ $oProducts->last_page }} </a> --}}

                        {{-- @php($nextPage = ($oProducts->current_page < $oProducts->last_page) ? $oProducts->current_page + 1 :  $oProducts->last_page) --}}
                        {{-- <a href="{{url('/products?page=' . $nextPage)}}" class="next-arrow"><i class="fa fa-arrow-right" aria-hidden="true"></i></a> --}}
                    {{-- </div> --}}
                </div>

                <!-- End Filter Bar -->
            </div>
            <!-- Strat side bar -->

            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head">Browse Categories</div>
                    <ul class="main-categories">
                        @foreach($aCategories as $oCategory)
                            <li class="main-nav-list"><a data-toggle="collapse" href="#{{str_replace(' ', '',$oCategory->category_name)}}" aria-expanded="false" aria-controls="{{str_replace(' ', '',$oCategory->category_name)}}"><span class="lnr lnr-arrow-right">{{$oCategory->category_name}}</span><span class="number">({{$oCategory->count_products}})</span></a>

                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="sidebar-filter mt-50">
                    <div class="top-filter-head">Product Filters</div>
                    <div class="common-filter">
                        <div class="head">Brands</div>
                        <form action="#">
                            <ul>
                                @foreach($aBrands as $oBrand)

                                    <li class="filter-list"><input class="pixel-radio" type="checkbox" checked="checked" id="{{$oBrand->brand_name}}" name="brand"><label for="apple">{{$oBrand->brand_name}}<span>({{$oBrand->count_products}})</span></label></li>
                                @endforeach
                            </ul>
                        </form>
                    </div>

                </div>
            </div>

            <!-- End Sidebar -->
        </div>
    </div>

@endsection


@section('footer')
    <script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

    <script>
        $(document).ready(function () {




            $(document).on("change", ".orderBy, .sortBy", function(e){
                var sortBy = $('.sortBy').val();
                var orderBy = $('.orderBy').val();
                var categoryId = $('.category-id').val()
                if(sortBy == '') {
                    return false;
                }

                window.location.href='/categories/'+categoryId+'/products?sortBy='+sortBy+'&orderBy='+ orderBy;
                return false;
            });


            $('.perPage').on('change', function(e){
                e.preventDefault();
                var perPage = $(this).val();
                var categoryId = $('.category-id').val()
                if(perPage == '') {
                    window.location.href = '/products';
                    return false;
                }
                window.location.href='/categories/'+categoryId+'/products?page=1&perPage=' + perPage;
                return false;
            });

            $(".wishList").click(function (e) {

                e.preventDefault();
                let productId = $(this).attr('data-id');

                $.ajax({
                    type: "GET",
                    data: {productId},
                    url : "wishlist/store",
                    success: function (response) {

                        let jsonResponse = JSON.parse(response);

                        console.log(jsonResponse.status);

                        if(jsonResponse.status == true)
                        {

                            Swal.fire({
                                text : "It's Added to your wishlist!",
                                title: 'Yayy!',
                                type : 'success'
                            })
                        }
                        else{

                            Swal.fire({
                                type : "info",
                                title: "Awww Snap!",
                                text : jsonResponse.message
                            })
                        }
                    },
                    error: function(jqXHR, exception){
                        if(jqXHR.status == 401) {
                            window.location.href = '/login';
                        }
                    }
                });
            })
        })
    </script>

@endsection
