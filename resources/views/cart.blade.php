@extends('layouts.master')

@section('head')
    <link rel="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" href="sweetalert2.min.css">

@endsection


@section('content')


<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
            <div class="col-first">
                <h1>Shopping Cart</h1>

            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->

<!-- Start Cart Area -->
<div class="container">
    <div class="cart-title">
        <div class="row">
            <div class="col-md-4">
                <h6 class="ml-15">Product</h6>
            </div>
            <div class="col-md-2">
                <h6>Price</h6>
            </div>
            <div class="col-md-2">
                <h6>Quantity</h6>
            </div>
            <div class="col-md-2">
                <h6>Total</h6>
            </div>
            <div class="col-md-2">
                <h6>Remove</h6>
            </div>
        </div>
    </div>
    <form method="post" id="updateCart" action="cart">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        @foreach($aCarts as $oCart)
    {{--        {{dd($oCart)}}--}}
            <div class="cart-single-item">
                <div class="row align-items-center">
                    <div class="col-md-4 col-12">
                        <div class="product-item d-flex align-items-center">
                            <img src="{{ isset($oCart->cart_product->image[0]->image_name) ? 'storage/thumbnail/'.$oCart->cart_product->image[0]->image_name: ''}} " class="img-fluid" alt="" />
                            <h6>{{$oCart->cart_product->product_title}}</h6>
                        </div>
                        <div class="product-item d-flex">
                            <small><b>Variant: </b>{{$oCart->variants ?? 'N/A'}}</small>
                        </div>
                    </div>
                    <div class="col-md-2 col-6">
                        <div class="price">$ {{$oCart->cart_product->price}}</div>
                    </div>
                    <div class="col-md-2 col-6">
                        <div class="quantity-container d-flex align-items-center mt-15">
                            <input type="text" name="qty[]" class="quantity-amount" value="{{$oCart->quantity_ordered}}" />
                            <input type="hidden" name="identifier[]" value="{{encrypt($oCart->id)}}" />
                            <div class="arrow-btn d-inline-flex flex-column">
                                <button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
                                <button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-12">
                        <div class="total">$ {{number_format($oCart->quantity_ordered * $oCart->cart_product->price)}}</div>
                    </div>
                    <div class="col-md-2 col-12 remove-cart">
                        <button type="button" data-id="{{$oCart->id}}" class="removeCart" style="color: grey;" onMouseOver="this.style.color='red'" onMouseOut="this.style.color='grey'" href="#"><i class="fa fa-trash"></i></button>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="cupon-area d-flex align-items-center justify-content-between flex-wrap">
            @if(isset($aCarts) && !empty($aCarts))
            <button type="submit" class="view-btn color-2"><span>Update Cart</span></button>
            <div class="cuppon-wrap d-flex align-items-center flex-wrap">

                <a class="view-btn color-2" href="cart/checkout"><span>Continue </span><i class="fa fa-arrow"></i></a>

            </div>
                @else
                <div class="">
                    <b>Your cart is empty</b>
                </div>
            @endif
        </div>
    </form>
    <div class="subtotal-area d-flex align-items-center justify-content-end">
        <div class="title text-uppercase">Subtotal</div>
        <div class="subtotal">${{$iGrandTotal}}</div>
    </div>


</div>


@endsection

@section('footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>


    <script>

        $(function () {

            $("#updateCart").on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    type : 'PUT',
                    url: $(this).attr("action"),
                    data: $(this).serialize(),
                    success: function (response) {
                        var response = JSON.parse(response);
                       if(response.status == true) {
                           console.log(response.message)
                          var sweetAlert =  Swal.fire(
                               response.message,
                               'Thanks!',
                               'success'
                           );


                        setTimeout(function(){
                            window.location.reload();

                        }, 1000)

                       } else {
                           console.log(response.message)
                       }
                    },

                    error: function (err) {
                        console.log(err);
                    }
                })
            });


            $(".removeCart").on('click', function (e) {
                e.preventDefault();

                let cartId = $(this).attr('data-id');

                $.ajax({
                    type: 'DELETE',
                    url: "/cart",
                    data : {cartId, "_token": "<?php echo csrf_token() ?>"},

                    success: function (response) {
                        response = JSON.parse(response);

                        if(response.status == true) {
                            Swal.fire(
                                response.message,
                                'Thanks!',
                                'success'
                            );

                            setTimeout(function(){
                                window.location.reload();

                            }, 1000)
                        }
                    },
                    error: function (xhr, status) {
                        console.log(xhr, status);
                    }
                })
            })
        })
    </script>
@endsection