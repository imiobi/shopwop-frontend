@extends('layouts.master')


@section('head')
    <link rel="stylesheet"
          href="{{asset('admin-assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection


@section('content')

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center">
                <div class="col-first">
                    <h1>My Orders</h1>

                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!-- Start My Orders Area -->
    <div class="container">
        <div class="cart-title">
            <div class="row">
                <div class="col-md-4">
                    <h6>Order Number</h6>
                </div>
                <div class="col-md-2">
                    <h6>Order Placed</h6>
                </div>
                <div class="col-md-2">
                    <h6>Status</h6>
                </div>
                <div class="col-md-2">
                    <h6>Date Shipped</h6>
                </div>
                <div class="col-md-2">
                    <h6>Total Amount</h6>
                </div>
            </div>
        </div>

        @foreach($aOrders as $oOrder)

        <div class="cart-single-item">
            <div class="row align-items-center">
                <div class="col-md-4 col-12">
                    <div class="">
                        <h6>{{$oOrder->order_number}}</h6>
                    </div>

                </div>
                <div class="col-md-2 col-6">
                    <div class="">{{date('d F Y', strtotime($oOrder->created_at))}}</div>
                </div>
                <div class="col-md-2 col-6">
                    <div class="">{{$oOrder->status}}</div>
                </div>
                <div class="col-md-2 col-12">
                    <div class="">{{isset($oOrder->shipped_date) ? date('d F Y', strtotime($oOrder->shipped_date)) : "N/A"}}</div>
                </div>
                <div class="col-md-2 col-12">
                    <div class="">{{number_format($oOrder->total_amount)}}</div>
                </div>
            </div>
        </div>
        @endforeach


    </div>


@endsection


@section('footer')
    <script src="{{asset('admin-assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin-assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(function () {
            $('#productTable').DataTable({
                ordering: false
            })
        })
    </script>
@endsection